<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>客户厂商管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/shop/shopCustsup/">客户厂商列表</a></li>
		<shiro:hasPermission name="shop:shopCustsup:edit"><li><a href="${ctx}/shop/shopCustsup/form">客户厂商添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="shopCustsup" action="${ctx}/shop/shopCustsup/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>名称：</label>
				<form:input path="name" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>编号：</label>
				<form:input path="number" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>地址：</label>
				<form:input path="address" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>联系人：</label>
				<form:input path="person" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>备注：</label>
				<form:input path="remarks" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>电话：</label>
				<form:input path="mobile" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>类型：</label>
				<form:select path="ctype" class="input-medium">
					<form:option value="" label="全部"/>
					<form:options items="${fns:getDictList('suppliercustomer')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>名称</th>
				<th>编号</th>
				<th>地址</th>
				<th>联系人</th>
				<th>备注</th>
				<th>电话</th>
				<th>税号</th>
				<th>开户银行</th>
				<th>银行帐号</th>
				<th>类型</th>
				<shiro:hasPermission name="shop:shopCustsup:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="shopCustsup">
			<tr>
				<td><a href="${ctx}/shop/shopCustsup/form?id=${shopCustsup.id}">
					${shopCustsup.name}
				</a></td>
				<td>
					${shopCustsup.number}
				</td>
				<td>
					${shopCustsup.address}
				</td>
				<td>
					${shopCustsup.person}
				</td>
				<td>
					${shopCustsup.remarks}
				</td>
				<td>
					${shopCustsup.mobile}
				</td>
				<td>
					${shopCustsup.taxid}
				</td>
				<td>
					${shopCustsup.bank}
				</td>
				<td>
					${shopCustsup.banknumber}
				</td>
				
				<td>
					${fns:getDictLabel(shopCustsup.ctype, 'suppliercustomer', '')}
				</td>
				<shiro:hasPermission name="shop:shopCustsup:edit"><td>
    				<a href="${ctx}/shop/shopCustsup/form?id=${shopCustsup.id}">修改</a>
					<a href="${ctx}/shop/shopCustsup/delete?id=${shopCustsup.id}" onclick="return confirmx('确认要删除该客户厂商吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>