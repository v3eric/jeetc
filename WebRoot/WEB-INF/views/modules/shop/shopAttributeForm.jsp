<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>产品分类管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
		function addRow(list, idx, tpl, row){
			$(list).append(Mustache.render(tpl, {
				idx: idx, delBtn: true, row: row
			}));
			$(list+idx).find("select").each(function(){
				$(this).val($(this).attr("data-value"));
			});
			$(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
				var ss = $(this).attr("data-value").split(',');
				for (var i=0; i<ss.length; i++){
					if($(this).val() == ss[i]){
						$(this).attr("checked","checked");
					}
				}
			});
		}
		function delRow(obj, prefix){
			var id = $(prefix+"_id");
			var delFlag = $(prefix+"_delFlag");
			if (id.val() == ""){
				$(obj).parent().parent().remove();
			}else if(delFlag.val() == "0"){
				delFlag.val("1");
				$(obj).html("&divide;").attr("title", "撤销删除");
				$(obj).parent().parent().addClass("error");
			}else if(delFlag.val() == "1"){
				delFlag.val("0");
				$(obj).html("&times;").attr("title", "删除");
				$(obj).parent().parent().removeClass("error");
			}
		}
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/shop/shopAttribute/">产品分类列表</a></li>
		<li class="active"><a href="${ctx}/shop/shopAttribute/form?id=${shopAttribute.id}">产品分类<shiro:hasPermission name="shop:shopAttribute:edit">${not empty shopAttribute.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="shop:shopAttribute:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="shopAttribute" action="${ctx}/shop/shopAttribute/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">属性名称：</label>
			<div class="controls">
				<form:input path="name" htmlEscape="false" maxlength="128" class="input-xlarge "/>
			</div>
		</div>
		
		
		
		
		
		<div class="control-group">
			<label class="control-label">分类id:</label>
			<div class="controls">
				<sys:treeselect id="shopCategory" name="shopCategory.id" value="${shopAttribute.shopCategory.id}" labelName="shopAttribute.shopCategory.name" labelValue="${shopAttribute.shopCategory.name}"
					title="分类id" url="/shop/shopCategory/treeData" extId="${shopCategory.id}" cssClass="" allowClear="true"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">输入类型：</label>
			<div class="controls">
				<form:select path="inputtype" class="input-xlarge ">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('shopinputtype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
		</div>
		
		
		<div class="control-group">
			<label class="control-label">所属类别：</label>
			<div class="controls">
			     <select  name="shopPrdatt" id="shopPrdatt"  style="vertical-align:top;">
				<option value="">请选择</option>
				<c:forEach items="${shopPrdattList}" var="b">
					<option value="${b.id }"  <c:if test="${b.id==shopAttribute.shopPrdatt.id}"> selected="selected" </c:if> >${b.attname}</option>
				</c:forEach>
				</select>
			</div>
		</div>
	
		<div class="control-group">
			<label class="control-label">是否搜索：</label>
			<div class="controls">
				<form:select path="issearch" class="input-xlarge ">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
		</div>
		
		
		
		<div class="control-group">
			<label class="control-label">状态：</label>
			<div class="controls">
				<form:input path="state" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">排序：</label>
			<div class="controls">
				<form:input path="sort" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		
			<div class="control-group">
				<label class="control-label">属性数值：</label>
				<div class="controls">
					<table id="contentTable" class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th class="hide"></th>
								<th>属性值中间用-分割</th>
								<th>状态</th>
								<th>排序</th>
							
								<shiro:hasPermission name="shop:shopAttribute:edit"><th width="10">&nbsp;</th></shiro:hasPermission>
							</tr>
						</thead>
						<tbody id="shopAttrivalueList">
						</tbody>
						<shiro:hasPermission name="shop:shopAttribute:edit"><tfoot>
							<tr><td colspan="6"><a href="javascript:" onclick="addRow('#shopAttrivalueList', shopAttrivalueRowIdx, shopAttrivalueTpl);shopAttrivalueRowIdx = shopAttrivalueRowIdx + 1;" class="btn">新增</a></td></tr>
						</tfoot></shiro:hasPermission>
					</table>
					<script type="text/template" id="shopAttrivalueTpl">//<!--
						<tr id="shopAttrivalueList{{idx}}">
							<td class="hide">
								<input id="shopAttrivalueList{{idx}}_id" name="shopAttrivalueList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
								<input id="shopAttrivalueList{{idx}}_delFlag" name="shopAttrivalueList[{{idx}}].delFlag" type="hidden" value="0"/>
							</td>
							<td>
								<input id="shopAttrivalueList{{idx}}_name" name="shopAttrivalueList[{{idx}}].name" type="text" value="{{row.name}}" maxlength="256" class="input-small "/>
							</td>
							<td>
								<input id="shopAttrivalueList{{idx}}_state" name="shopAttrivalueList[{{idx}}].state" type="text" value="{{row.state}}" maxlength="255" class="input-small "/>
							</td>
							<td>
								<input id="shopAttrivalueList{{idx}}_sort" name="shopAttrivalueList[{{idx}}].sort" type="text" value="{{row.sort}}" maxlength="64" class="input-small "/>
							</td>
						
							<shiro:hasPermission name="shop:shopAttribute:edit"><td class="text-center" width="10">
								{{#delBtn}}<span class="close" onclick="delRow(this, '#shopAttrivalueList{{idx}}')" title="删除">&times;</span>{{/delBtn}}
							</td></shiro:hasPermission>
						</tr>//-->
					</script>
					<script type="text/javascript">
						var shopAttrivalueRowIdx = 0, shopAttrivalueTpl = $("#shopAttrivalueTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
						$(document).ready(function() {
							var data = ${fns:toJson(shopAttribute.shopAttrivalueList)};
							for (var i=0; i<data.length; i++){
								addRow('#shopAttrivalueList', shopAttrivalueRowIdx, shopAttrivalueTpl, data[i]);
								shopAttrivalueRowIdx = shopAttrivalueRowIdx + 1;
							}
						});
					</script>
				</div>
			</div>
		<div class="form-actions">
			<shiro:hasPermission name="shop:shopAttribute:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>