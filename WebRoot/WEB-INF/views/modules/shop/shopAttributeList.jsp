<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>产品分类管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/shop/shopAttribute/">产品分类列表</a></li>
		<shiro:hasPermission name="shop:shopAttribute:edit"><li><a href="${ctx}/shop/shopAttribute/form">产品分类添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="shopAttribute" action="${ctx}/shop/shopAttribute/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>属性名称：</label>
				<form:input path="name" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>所属分类：</label>
				<form:input path="shopCategory" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>属性名称</th>
				<th>属性分类</th>
				<th>属性类别</th>
				
				<th>是否搜索</th>
				<th>update_date</th>
			
				<shiro:hasPermission name="shop:shopAttribute:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="shopAttribute">
			<tr>
				<td><a href="${ctx}/shop/shopAttribute/form?id=${shopAttribute.id}">
					${shopAttribute.name}
				</a></td>
				<td>
					${shopAttribute.shopPrdatt.attname}
				</td>
			
				<td>
					${shopAttribute.shopCategory.name}
				</td>
				<td>
					${fns:getDictLabel(shopAttribute.issearch, 'yes_no', '')}
				</td>
				<td>
					<fmt:formatDate value="${shopAttribute.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				
				<shiro:hasPermission name="shop:shopAttribute:edit"><td>
    				<a href="${ctx}/shop/shopAttribute/form?id=${shopAttribute.id}">修改</a>
					<a href="${ctx}/shop/shopAttribute/delete?id=${shopAttribute.id}" onclick="return confirmx('确认要删除该产品分类吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>