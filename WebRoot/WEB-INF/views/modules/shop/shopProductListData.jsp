<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>商品资料管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
	
		$(document).ready(function() {
		
			$('#queding').on('click', function(){
				
				// alert(parent.$("tr[id^='shopStockdetailList']").length);
				 parent.addrownum = parent.$("tr[id^='shopStockdetailList']").length;
				 $('input[name="ids"]:checked').each(function(){
					 	var clen=$('input[name="ids"]:checked').length;
					 //	alert(clen);
					 	var cid=$.trim($(this).val());    //获取id
					 	var cname=$.trim($(this).parent().next().text());//获取名字
		                var cnum=$.trim($(this).parent().next().next().next().next().next().next().text());//获取编号
		              //  parent.$('#parentIframe').text(clen+cid+cname+cnum);
		             // parent.layer.tips('Look here', '#parentIframe', {time: 5000});	    
		                parent.$('#addrowshop').click();//弹窗中选择几行数据下面添加几行
		                parent.$("#shopStockdetailList"+parent.addrownum+"_name").attr("value",cname);
		                parent.$("#shopStockdetailList"+parent.addrownum+"_num").attr("data-value",cid);
		                parent.$("#shopStockdetailList"+parent.addrownum+"_num").find('option[value="'+cid+'"]').attr("selected",true);
		              //  parent.$(" select option[value='"+cnum+"']").attr("select","selected");  //如果值一样 就选中对应的option,

		                parent.addrownum=parent.addrownum+1;
		                
		                });
				  //当你在iframe页面关闭自身时
				    
	  			    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
	  			    parent.layer.close(index); //再执行关闭   
	  			 
			 

			});
		});
		
		
		
		
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	
	
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/shop/shopProduct/">商品资料列表</a></li>
		<li class="active"></li>
		
	<!--  	<shiro:hasPermission name="shop:shopProduct:edit"><li><a href="${ctx}/shop/shopProduct/form">商品资料添加</a></li></shiro:hasPermission>-->
	</ul>
	<form:form id="searchForm" modelAttribute="shopProduct" action="${ctx}/shop/shopProduct/listdata" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>名称：</label>
				<form:input path="pname" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>所属品牌：</label>
			<select  name="shopBrand" id="shopBrand"  style="vertical-align:top;">
				<option value="">请选择</option>
				<c:forEach items="${shopBrandList}" var="b">
					<option value="${b.id }" >${b.cnname}</option>
				</c:forEach>
			</select>
			</li>
			<li><label>所属分类：</label>
				<sys:treeselect id="shopCategory" name="shopCategory.id" value="${shopProduct.shopCategory.id}" labelName="shopCategory.name" labelValue="${shopProduct.shopCategory.name}"
					title="分类id" url="/shop/shopCategory/treeData" module="shopProduct" cssClass="required"/>
			</li>
			<li><label>列表价格：</label>
				<form:input path="price" htmlEscape="false" class="input-medium"/>
			</li>
			<li><label>数量：</label>
				<form:input path="qty" htmlEscape="false" class="input-medium"/>
			</li>
			<li><label>商家编码：</label>
				<form:input path="productnum" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li><a id="queding" class="btn">确定选择</a></li>
			
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>全选</th>
				<th>名称</th>
				<th>所属品牌</th>
				<th>所属分类</th>
				<th>列表价格</th>
				<th>数量</th>
				<th>商家编码</th>
				<th>update_date</th>
				<th>remarks</th>
				<shiro:hasPermission name="shop:shopProduct:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="shopProduct">
			<tr>
				<td><input type="checkbox" name="ids" id="ids" value="${shopProduct.id}" /></td>
				<td><a href="${ctx}/shop/shopProduct/form?id=${shopProduct.id}">
					${shopProduct.pname}
				</a></td>
				<td>
					${shopProduct.shopBrand.cnname}
				</td>
				<td>
					${shopProduct.shopCategory.name}
				</td>
				<td>
					${shopProduct.price}
				</td>
				<td>
					${shopProduct.qty}
				</td>
				<td>
					${shopProduct.productnum}
				</td>
				<td>
					<fmt:formatDate value="${shopProduct.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${shopProduct.remarks}
				</td>
				<shiro:hasPermission name="shop:shopProduct:edit"><td>
    				<a href="${ctx}/shop/shopProduct/form?id=${shopProduct.id}">修改</a>
					<a href="${ctx}/shop/shopProduct/delete?id=${shopProduct.id}" onclick="return confirmx('确认要删除该商品资料吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>