<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>产品保存成功管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/shop/shopGoods/">产品保存成功列表</a></li>
		<shiro:hasPermission name="shop:shopGoods:edit"><li><a href="${ctx}/shop/shopGoods/form">产品保存成功添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="shopGoods" action="${ctx}/shop/shopGoods/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>名称：</label>
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>备注：</label>
				<form:input path="rem" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>价格：</label>
				<form:input path="price" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>名称</th>
				<th>备注</th>
				<th>价格</th>
				<th>数量</th>
				<th>编号</th>
				<th>图片</th>
				<shiro:hasPermission name="shop:shopGoods:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="shopGoods">
			<tr>
				<td><a href="${ctx}/shop/shopGoods/form?id=${shopGoods.id}">
					${shopGoods.name}
				</a></td>
				<td>
					${shopGoods.rem}
				</td>
				<td>
					${shopGoods.price}
				</td>
				<td>
					${shopGoods.qty}
				</td>
				<td>
					${shopGoods.number}
				</td>
				<td>
					${shopGoods.picture}
				</td>
				<shiro:hasPermission name="shop:shopGoods:edit"><td>
    				<a href="${ctx}/shop/shopGoods/form?id=${shopGoods.id}">修改</a>
					<a href="${ctx}/shop/shopGoods/delete?id=${shopGoods.id}" onclick="return confirmx('确认要删除该产品保存成功吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>