<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>库存管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript" src="${ctxStatic}/modules/shop/shopStockqtyList.js"></script>
	<script type="text/javascript">
		
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/shop/shopStock/">库存列表</a></li>
		<shiro:hasPermission name="shop:shopStock:edit"><li><a href="${ctx}/shop/shopStock/form">库存添加</a></li></shiro:hasPermission>
	</ul>
	
	<sys:message content="${message}"/>
	<div id="total" style="color: green;"></div>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>名称</th>
				<th>数量</th>
				<th>价格</th>
				<th>小计</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${shopStockqtyList}" var="shopStockqty">
			<tr>
			
				<td>${shopStockqty.name}</td>
				<td>${shopStockqty.qty}</td>
				<td>${shopStockqty.price}</td>
				<td>${shopStockqty.salprice}</td>
			
			</tr>
		</c:forEach>
		</tbody>
	</table>

</body>
</html>