<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>品牌管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/shop/shopBrand/">品牌列表</a></li>
		<shiro:hasPermission name="shop:shopBrand:edit"><li><a href="${ctx}/shop/shopBrand/form">品牌添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="shopBrand" action="${ctx}/shop/shopBrand/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>中文名称：</label>
				<form:input path="cnname" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>英文名称：</label>
				<form:input path="enname" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>描述：</label>
				<form:input path="description" htmlEscape="false" maxlength="256" class="input-medium"/>
			</li>
			<li><label>官网地址：</label>
				<form:input path="websiteurl" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>品牌故事：</label>
				<form:input path="brandstory" htmlEscape="false" maxlength="1024" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>中文名称</th>
				<th>英文名称</th>
				<th>描述</th>
				<th>logo</th>
				<th>状态</th>
				<th>官网地址</th>
				<th>品牌故事</th>
				<th>update_date</th>
				<th>标记</th>
				<shiro:hasPermission name="shop:shopBrand:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="shopBrand">
			<tr>
				<td><a href="${ctx}/shop/shopBrand/form?id=${shopBrand.id}">
					${shopBrand.cnname}
				</a></td>
				<td>
					${shopBrand.enname}
				</td>
				<td>
					${shopBrand.description}
				</td>
				<td>
					${shopBrand.logo}
				</td>
				<td>
					${shopBrand.state}
				</td>
				<td>
					${shopBrand.websiteurl}
				</td>
				<td>
					${shopBrand.brandstory}${shopBrand.shopCategory.name}
				</td>
				<td>
					<fmt:formatDate value="${shopBrand.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${shopBrand.remarks}
				</td>
				<shiro:hasPermission name="shop:shopBrand:edit"><td>
    				<a href="${ctx}/shop/shopBrand/form?id=${shopBrand.id}">修改</a>
					<a href="${ctx}/shop/shopBrand/delete?id=${shopBrand.id}" onclick="return confirmx('确认要删除该品牌吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>