<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>产品特征码管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/shop/shopPrdmark/">产品特征码列表</a></li>
		<shiro:hasPermission name="shop:shopPrdmark:edit"><li><a href="${ctx}/shop/shopPrdmark/form">产品特征码添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="shopPrdmark" action="${ctx}/shop/shopPrdmark/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>特征名称：</label>
				<form:input path="markname" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>特征编码：</label>
				<form:input path="marknumber" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			
			<li><label>分类名称：</label>
				<form:input path="shopPrdmarkcategory" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>特征名称</th>
				<th>特征编码</th>
			
				<th>分类名称</th>
				<th>update_date</th>
				<th>remarks</th>
				<shiro:hasPermission name="shop:shopPrdmark:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="shopPrdmark">
			<tr>
				<td><a href="${ctx}/shop/shopPrdmark/form?id=${shopPrdmark.id}">
					${shopPrdmark.markname}
				</a></td>
				<td>
					${shopPrdmark.marknumber}
				</td>
				
				<td>
					${shopPrdmark.shopPrdmarkcategory.pname}
				</td>
				<td>
					<fmt:formatDate value="${shopPrdmark.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${shopPrdmark.remarks}
				</td>
				<shiro:hasPermission name="shop:shopPrdmark:edit"><td>
    				<a href="${ctx}/shop/shopPrdmark/form?id=${shopPrdmark.id}">修改</a>
					<a href="${ctx}/shop/shopPrdmark/delete?id=${shopPrdmark.id}" onclick="return confirmx('确认要删除该产品特征码吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>