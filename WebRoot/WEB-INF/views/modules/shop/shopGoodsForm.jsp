<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>产品保存成功管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
		function addRow(list, idx, tpl, row){
			$(list).append(Mustache.render(tpl, {
				idx: idx, delBtn: true, row: row
			}));
			$(list+idx).find("select").each(function(){
				$(this).val($(this).attr("data-value"));
			});
			$(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
				var ss = $(this).attr("data-value").split(',');
				for (var i=0; i<ss.length; i++){
					if($(this).val() == ss[i]){
						$(this).attr("checked","checked");
					}
				}
			});
		}
		function delRow(obj, prefix){
			var id = $(prefix+"_id");
			var delFlag = $(prefix+"_delFlag");
			if (id.val() == ""){
				$(obj).parent().parent().remove();
			}else if(delFlag.val() == "0"){
				delFlag.val("1");
				$(obj).html("&divide;").attr("title", "撤销删除");
				$(obj).parent().parent().addClass("error");
			}else if(delFlag.val() == "1"){
				delFlag.val("0");
				$(obj).html("&times;").attr("title", "删除");
				$(obj).parent().parent().removeClass("error");
			}
		}
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/shop/shopGoods/">产品保存成功列表</a></li>
		<li class="active"><a href="${ctx}/shop/shopGoods/form?id=${shopGoods.id}">产品保存成功<shiro:hasPermission name="shop:shopGoods:edit">${not empty shopGoods.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="shop:shopGoods:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="shopGoods" action="${ctx}/shop/shopGoods/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">名称：</label>
			<div class="controls">
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注：</label>
			<div class="controls">
				
	          <select  name="rem" id="rem"  style="vertical-align:top;">
				<option value="请选择">请选择</option>
				<c:forEach items="${shopGoodsList}" var="b">
					<option value="${b.id }"  <c:if test="${b.id==shopGoods.rem}"> selected="selected" </c:if> >${b.name}</option>
				</c:forEach>
				</select>
				
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">价格：</label>
			<div class="controls">
				<form:input path="price" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">数量：</label>
			<div class="controls">
				<form:input path="qty" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">编号：</label>
			<div class="controls">
				<form:input path="number" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">图片：</label>
			<div class="controls">
				<form:hidden id="picture" path="picture" htmlEscape="false" maxlength="255" class="input-xlarge"/>
				<sys:ckfinder input="picture" type="files" uploadPath="/shop/shopGoods" selectMultiple="true"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">标记：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
			<div class="control-group">
				<label class="control-label">商品明细：</label>
				<div class="controls">
					<table id="contentTable" class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th class="hide"></th>
								<th>名称</th>
								<th>编号</th>
								<th>销货数量</th>
								<th>价格</th>
								<th>销货价格</th>
								<th>备注</th>
								<th>标记</th>
								<shiro:hasPermission name="shop:shopGoods:edit"><th width="10">&nbsp;</th></shiro:hasPermission>
							</tr>
						</thead>
						<tbody id="shopGoodetailList">
						</tbody>
						<shiro:hasPermission name="shop:shopGoods:edit"><tfoot>
							<tr><td colspan="9"><a href="javascript:" onclick="addRow('#shopGoodetailList', shopGoodetailRowIdx, shopGoodetailTpl);shopGoodetailRowIdx = shopGoodetailRowIdx + 1;" class="btn">新增</a></td></tr>
						</tfoot></shiro:hasPermission>
					</table>
					
					<script type="text/javascript" id="shopGoodetailTpl">
						<tr id="shopGoodetailList{{idx}}">
							<td class="hide">
								<input id="shopGoodetailList{{idx}}_id" name="shopGoodetailList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
								<input id="shopGoodetailList{{idx}}_delFlag" name="shopGoodetailList[{{idx}}].delFlag" type="hidden" value="0"/>
							</td>
							
							<td>
							<select id="shopGoodetailList{{idx}}_name" name="shopGoodetailList[{{idx}}].name" data-value="{{row.name}}" class="input-small ">
							<option value=""></option>
							<c:forEach items="${shopGoodsList}" var="dict">
								<option value="${dict.id}">${dict.name}${dict.price}</option>
							</c:forEach>
						</select>

							</td>
							<td>
								<input id="shopGoodetailList{{idx}}_number" name="shopGoodetailList[{{idx}}].number" type="text" value="{{row.number}}" maxlength="255" class="input-small "/>
							</td>
							<td>
								<input id="shopGoodetailList{{idx}}_qty" name="shopGoodetailList[{{idx}}].qty" type="text" value="{{row.qty}}" maxlength="10" class="input-small "/>
							</td>
							<td>
								<input id="shopGoodetailList{{idx}}_price" name="shopGoodetailList[{{idx}}].price" type="text" value="{{row.price}}" maxlength="10" class="input-small "/>
							</td>
							<td>
								<input id="shopGoodetailList{{idx}}_salprice" name="shopGoodetailList[{{idx}}].salprice" type="text" value="{{row.salprice}}" maxlength="10" class="input-small "/>
							</td>
							<td>
								<input id="shopGoodetailList{{idx}}_rem" name="shopGoodetailList[{{idx}}].rem" type="text" value="{{row.rem}}" maxlength="255" class="input-small "/>
							</td>
							<td>
								<textarea id="shopGoodetailList{{idx}}_remarks" name="shopGoodetailList[{{idx}}].remarks" rows="4" maxlength="255" class="input-small ">{{row.remarks}}</textarea>
							</td>
							<shiro:hasPermission name="shop:shopGoods:edit"><td class="text-center" width="10">
								{{#delBtn}}<span class="close" onclick="delRow(this, '#shopGoodetailList{{idx}}')" title="删除">&times;</span>{{/delBtn}}
							</td></shiro:hasPermission>
						</tr>
					</script>
					<script type="text/javascript">
						var shopGoodetailRowIdx = 0, shopGoodetailTpl = $("#shopGoodetailTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
						$(document).ready(function() {
							var data = ${fns:toJson(shopGoods.shopGoodetailList)};
							for (var i=0; i<data.length; i++){
								addRow('#shopGoodetailList', shopGoodetailRowIdx, shopGoodetailTpl, data[i]);
								shopGoodetailRowIdx = shopGoodetailRowIdx + 1;
							}
						});
					</script>
				</div>
			</div>
		<div class="form-actions">
			<shiro:hasPermission name="shop:shopGoods:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>