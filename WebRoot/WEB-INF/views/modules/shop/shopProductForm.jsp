<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>商品资料管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript" src="${ctxStatic}/modules/shop/shopProductForm.js"></script>
	
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/shop/shopProduct/">商品资料列表</a></li>
		<li class="active"><a href="${ctx}/shop/shopProduct/form?id=${shopProduct.id}">商品资料<shiro:hasPermission name="shop:shopProduct:edit">${not empty shopProduct.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="shop:shopProduct:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="shopProduct" action="${ctx}/shop/shopProduct/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		
		<div class="control-group">
			基本信息
		</div>
		<div class="control-group">
			<label class="control-label">名称：</label>
			<div class="controls">
				<form:input path="pname" htmlEscape="false" maxlength="128" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">所属品牌：</label>
			<div class="controls">
			     <select  name="shopBrand" id="shopBrand"  style="vertical-align:top;">
				<option value="">请选择</option>
				<c:forEach items="${shopBrandList}" var="b">
					<option value="${b.id }"  <c:if test="${b.id==shopProduct.shopBrand.id}"> selected="selected" </c:if> >${b.cnname}</option>
				</c:forEach>
				</select>
			</div>
		</div>
			<div class="control-group">
			<label class="control-label">分类id:</label>
			<div class="controls">
				<sys:treeselect id="shopCategory" name="shopCategory.id" value="${shopProduct.shopCategory.id}" labelName="shopCategory.name" labelValue="${shopProduct.shopCategory.name}"
					title="分类id" url="/shop/shopCategory/treeData" module="shopProduct" cssClass="required"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">列表价格：</label>
			<div class="controls">
				<form:input path="price" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		

		
		<div class="control-group">
			<label class="control-label">宝贝卖点：</label>
			<div class="controls">
				<form:input path="focus" htmlEscape="false" maxlength="512" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">数量：</label>
			<div class="controls">
				<form:input path="qty" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">商家编码：</label>
			<div class="controls">
				<form:input path="productnum" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">上架类型:：</label>
			<div class="controls">
				<form:select path="ongoodstype" class="input-xlarge ">
					<form:option value="" label="请选择"/>
					<form:options items="${fns:getDictList('shop_putonsaletype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">上架时间：</label>
			<div class="controls">
				<input name="ongoodstypetime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${shopProduct.ongoodstypetime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">体积：</label>
			<div class="controls">
				<form:input path="volume" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">重量：</label>
			<div class="controls">
				<form:input path="weight" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">宝贝所在地区：</label>
			<div class="controls">
				<form:input path="sysAreaId" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">快递模版：</label>
			<div class="controls">
				<form:input path="expressId" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">remarks：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
		
		
		  <div class="control-group">
			<label class="control-label">特征模版：</label>
			<div class="controls">
			     <select  name="shopPrdmarkcategory" id="shopPrdmarkcategory"  style="vertical-align:top;">
				<option value="">请选择</option>
				<c:forEach items="${shopPrdmarkCategoryList}" var="d">
					<option value="${d.id }"  <c:if test="${d.id==shopProduct.shopPrdmarkcategory.id}"> selected="selected" </c:if> >${d.pname}</option>
				</c:forEach>
				</select>
			</div>
		</div>
		
			
		<div class="control-group">
			<label class="control-label">所属类别：</label>
			<div class="controls">
			     <select  name="shopPrdatt" id="shopPrdatt"  style="vertical-align:top;">
				<option value="">请选择</option>
				<c:forEach items="${shopPrdattList}" var="c">
					<option value="${c.id }"  <c:if test="${c.id==shopProduct.shopPrdatt.id}"> selected="selected" </c:if> >${c.attname}</option>
				</c:forEach>
				</select>
			</div>
		</div>
			
		
		<div class="form-actions">
			<shiro:hasPermission name="shop:shopProduct:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>