<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>产品特征码管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/shop/shopPrdmark/">产品特征码列表</a></li>
		<li class="active"><a href="${ctx}/shop/shopPrdmark/form?id=${shopPrdmark.id}">产品特征码<shiro:hasPermission name="shop:shopPrdmark:edit">${not empty shopPrdmark.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="shop:shopPrdmark:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="shopPrdmark" action="${ctx}/shop/shopPrdmark/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">特征名称：</label>
			<div class="controls">
				<form:input path="markname" htmlEscape="false" maxlength="128" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">特征编码：</label>
			<div class="controls">
				<form:input path="marknumber" htmlEscape="false" maxlength="128" class="input-xlarge "/>
			</div>
		</div>
	
	  <div class="control-group">
			<label class="control-label">分类名称：</label>
			<div class="controls">
			     <select  name="shopPrdmarkcategory" id="shopPrdmarkcategory"  style="vertical-align:top;">
				<option value="">请选择</option>
				<c:forEach items="${shopPrdmarkCategoryList}" var="b">
					<option value="${b.id }"  <c:if test="${b.id==shopPrdmark.shopPrdmarkcategory.id}"> selected="selected" </c:if> >${b.pname}</option>
				</c:forEach>
				</select>
			</div>
		</div>
	
	
		
		
		
		<div class="control-group">
			<label class="control-label">remarks：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="shop:shopPrdmark:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>