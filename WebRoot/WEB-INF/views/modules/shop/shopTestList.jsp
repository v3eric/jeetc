<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>测试管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/shop/shopTest/">测试列表</a></li>
		<shiro:hasPermission name="shop:shopTest:edit"><li><a href="${ctx}/shop/shopTest/form">测试添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="shopTest" action="${ctx}/shop/shopTest/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>分类ID：</label>
				<form:input path="shopCategoryId" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>中文名称：</label>
				<form:input path="cnname" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>英文名称：</label>
				<form:input path="enname" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>描述：</label>
				<form:input path="description" htmlEscape="false" maxlength="256" class="input-medium"/>
			</li>
			<li><label>logo：</label>
				<form:input path="logo" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>状态：</label>
				<form:input path="state" htmlEscape="false" maxlength="1" class="input-medium"/>
			</li>
			<li><label>官网地址：</label>
				<form:input path="websiteurl" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>品牌故事：</label>
				<form:input path="brandstory" htmlEscape="false" maxlength="1024" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>分类ID</th>
				<th>中文名称</th>
				<th>英文名称</th>
				<th>描述</th>
				<th>logo</th>
				<th>状态</th>
				<th>官网地址</th>
				<th>品牌故事</th>
				<th>update_date</th>
				<th>标记</th>
				<shiro:hasPermission name="shop:shopTest:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="shopTest">
			<tr>
				<td><a href="${ctx}/shop/shopTest/form?id=${shopTest.id}">
					${shopTest.shopCategoryId}
				</a></td>
				<td>
					${shopTest.cnname}
				</td>
				<td>
					${shopTest.enname}
				</td>
				<td>
					${shopTest.description}
				</td>
				<td>
					${shopTest.logo}
				</td>
				<td>
					${shopTest.state}
				</td>
				<td>
					${shopTest.websiteurl}
				</td>
				<td>
					${shopTest.brandstory}
				</td>
				<td>
					<fmt:formatDate value="${shopTest.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${shopTest.remarks}
				</td>
				<shiro:hasPermission name="shop:shopTest:edit"><td>
    				<a href="${ctx}/shop/shopTest/form?id=${shopTest.id}">修改</a>
					<a href="${ctx}/shop/shopTest/delete?id=${shopTest.id}" onclick="return confirmx('确认要删除该测试吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>