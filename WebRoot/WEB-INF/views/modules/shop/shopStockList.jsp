<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>库存管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/shop/shopStock/">库存列表</a></li>
		<shiro:hasPermission name="shop:shopStock:edit"><li><a href="${ctx}/shop/shopStock/form">库存添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="shopStock" action="${ctx}/shop/shopStock/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>日期：</label>
				<input name="beginJhdate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${shopStock.beginJhdate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endJhdate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${shopStock.endJhdate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li><label>备注：</label>
				<form:input path="rem" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>进货厂商：</label>
				<form:input path="custsupp.id" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>业务人员：</label>
				<form:input path="person" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>单据类别：</label>
				<form:select path="cktype" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('shop_cktype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>单号：</label>
				<form:input path="number" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>日期</th>
				<th>备注</th>
				<th>客户厂商</th>
				<th>业务人员</th>
				<th>单据类别</th>
				<th>单号</th>
				<th>更新日期</th>
				<th>备注</th>
				<shiro:hasPermission name="shop:shopStock:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="shopStock">
			<tr>
				<td><a href="${ctx}/shop/shopStock/form?id=${shopStock.id}">
					<fmt:formatDate value="${shopStock.jhdate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</a></td>
				<td>
					${shopStock.rem}
				</td>
				<td>
						${shopStock.custsupp.name}
					
				</td>
				<td>
					${shopStock.person}
				</td>
				<td>
					${fns:getDictLabel(shopStock.cktype, 'shop_cktype', '')}
				</td>
				<td>
					${shopStock.number}
				</td>
				<td>
					<fmt:formatDate value="${shopStock.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${shopStock.remarks}
				</td>
				<shiro:hasPermission name="shop:shopStock:edit"><td>
    				<a href="${ctx}/shop/shopStock/form?id=${shopStock.id}">修改</a>
					<a href="${ctx}/shop/shopStock/delete?id=${shopStock.id}" onclick="return confirmx('确认要删除该库存吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>