<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>库存管理</title>
	<meta name="decorator" content="default"/>
	<script src="${ctxStatic}/layer/layer.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		
	});

	var addrownum = $("tr[id^='shopStockdetailList']").length;//id属性以shopStockdetailList开始的所有tr标签	
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
			  $('#parentIframe').on('click', function(){
			
			        layer.open({
			            type: 2,
			            title: '产品资料',
			            maxmin: true,
			            shadeClose: true, //点击遮罩关闭层
			            area : ['800px' , '520px'],
			            content: '${ctx}/shop/shopProduct/listdata'
			        });
			    });
			  
		});
		function addRow(list, idx, tpl, row){
			$(list).append(Mustache.render(tpl, {
				idx: idx, delBtn: true, row: row
			}));
			$(list+idx).find("select").each(function(){
				$(this).val($(this).attr("data-value"));
			});
			$(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
				var ss = $(this).attr("data-value").split(',');
				for (var i=0; i<ss.length; i++){
					if($(this).val() == ss[i]){
						$(this).attr("checked","checked");
					}
				}
			});
		}
		function delRow(obj, prefix){
			var id = $(prefix+"_id");
			var delFlag = $(prefix+"_delFlag");
			if (id.val() == ""){
				$(obj).parent().parent().remove();
			}else if(delFlag.val() == "0"){
				delFlag.val("1");
				$(obj).html("&divide;").attr("title", "撤销删除");
				$(obj).parent().parent().addClass("error");
			}else if(delFlag.val() == "1"){
				delFlag.val("0");
				$(obj).html("&times;").attr("title", "删除");
				$(obj).parent().parent().removeClass("error");
			}
		}
		
		
		function setprdname(th,idx){
			var ss = $(th).find("option:selected").text();
			//alert(ss);
		 	$("#shopStockdetailList"+idx+"_name").val(ss);
		}
	
		
		<!--
	    function show(obj){
	      
	        // document.getElementById("d").innerHTML = "显示需要组装的信息";
	        // window.open ("page.html", "newwindow", "height=100, width=400, toolbar= no, menubar=no, scrollbars=no, resizable=no, location=no, status=no") 
	    //window.open('index2.jsp?p1=2','_blank');
	        var x = window.screen.height;
	    var y = window.screen.width;
	    var h = 500;
	    var w = 800;
	    var model = "title=word,resizable=yes,scrollbars=yes,height=" + h + ",width=" + w + ",status=yes,toolbar=no,menubar=no,location=no,top=" + (x-h)/2 + ",left=" + (y-w)/2;
	    var url = "";

	    url = "${ctx}/shop/shopProduct/listdata";//弹出窗口的页面内容
	    var oOpen = window.open(url,"adviceDetail",model);
	    oOpen.focus();
	    }
	    -->
	  
	</script>
	
	
	
</head>
<body>


	<ul class="nav nav-tabs">
		<li><a href="${ctx}/shop/shopStock/">库存列表</a></li>
		<li class="active"><a href="${ctx}/shop/shopStock/form?id=${shopStock.id}">库存<shiro:hasPermission name="shop:shopStock:edit">${not empty shopStock.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="shop:shopStock:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="shopStock" action="${ctx}/shop/shopStock/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">日期：</label>
			<div class="controls">
				<input name="jhdate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${shopStock.jhdate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注：</label>
			<div class="controls">
				<form:input path="rem" htmlEscape="false" maxlength="255" class="input-xlarge "/>
				<!-- <p onclick="show(this)"> 点击</p> -->
				
			</div>
		</div>
		 
		<div class="control-group">
			<label class="control-label">客户厂商：</label>
			<div class="controls">
			     <select  name="custsupp" id="custsupp"  style="vertical-align:top;">
				<option value="请选择">请选择</option>
				<c:forEach items="${shopCustsupList}" var="b">
					<option value="${b.id }"  <c:if test="${b.id==shopStock.custsupp.id}"> selected="selected" </c:if> >${b.name}</option>
				</c:forEach>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">业务人员：</label>
			<div class="controls">
				<form:input path="person" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">单据类别：</label>
			<div class="controls">
				<form:select path="cktype" class="input-xlarge ">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('shop_cktype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">单号：</label>
			<div class="controls">
				<form:input path="number" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">remarks：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
			<div class="control-group">
				<label class="control-label">出入库明细：</label>
				<div class="controls">
					<table id="contentTable" class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th class="hide"></th>
								<th>货品编号</th>
								<th>货品名称</th>
								<th>特征码</th>
								<th>数量</th>
								<th>单价</th>
								<th>小计算</th>
								<th>备注</th>
								<th>remarks</th>
								<shiro:hasPermission name="shop:shopStock:edit"><th width="10">&nbsp;</th></shiro:hasPermission>
							</tr>
						</thead>
						<tbody id="shopStockdetailList">
						</tbody>
						<shiro:hasPermission name="shop:shopStock:edit"><tfoot>
							<tr><td colspan="10"><a id="addrowshop" href="javascript:" onclick="addRow('#shopStockdetailList', shopStockdetailRowIdx, shopStockdetailTpl);shopStockdetailRowIdx = shopStockdetailRowIdx + 1;" class="btn">新增</a><a id="parentIframe" class="btn">选择产品</a></td></tr>
						</tfoot></shiro:hasPermission>
					</table>
					<script type="text/template" id="shopStockdetailTpl">//<!--
						<tr id="shopStockdetailList{{idx}}">
							<td class="hide">
								<input id="shopStockdetailList{{idx}}_id" name="shopStockdetailList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
								<input id="shopStockdetailList{{idx}}_delFlag" name="shopStockdetailList[{{idx}}].delFlag" type="hidden" value="0"/>
							</td>
							<td>
  <select style="width:400px" id="shopStockdetailList{{idx}}_num" name="shopStockdetailList[{{idx}}].shopProduct.id" data-value="{{row.shopProduct.id}}"  onclick="setprdname(this,{{idx}});">
							<option value=""></option>
							<c:forEach items="${shopProductList}" var="dict">
								<option value="${dict.id}">${dict.productnum}${dict.pname}</option>
							</c:forEach>
						</select>								

							</td>
							<td>
								<input id="shopStockdetailList{{idx}}_name" name="shopStockdetailList[{{idx}}].name" type="text" value="{{row.name}}" maxlength="255" class="input-small "/>
							</td>
							<td>
<select id="shopStockdetailList{{idx}}_name" name="shopStockdetailList[{{idx}}].shopPrdmark" data-value="{{row.shopPrdmark.id}}" class="input-small ">
							<option value=""></option>
							<c:forEach items="${shopPrdmarkList}" var="dict">
								<option value="${dict.id}">${dict.markname}</option>
							</c:forEach>
						</select>									


							</td>
							<td>
								<input id="shopStockdetailList{{idx}}_qty" name="shopStockdetailList[{{idx}}].qty" type="text" value="{{row.qty}}" maxlength="10" class="input-small "/>
							</td>
							<td>
								<input id="shopStockdetailList{{idx}}_price" name="shopStockdetailList[{{idx}}].price" type="text" value="{{row.price}}" maxlength="10" class="input-small "/>
							</td>
							<td>
								<input id="shopStockdetailList{{idx}}_salprice" name="shopStockdetailList[{{idx}}].salprice" type="text" value="{{row.salprice}}" maxlength="10" class="input-small "/>
							</td>
							<td>
								<input id="shopStockdetailList{{idx}}_rem" name="shopStockdetailList[{{idx}}].rem" type="text" value="{{row.rem}}" maxlength="255" class="input-small "/>
							</td>
							
							<shiro:hasPermission name="shop:shopStock:edit"><td class="text-center" width="10">
								{{#delBtn}}<span class="close" onclick="delRow(this, '#shopStockdetailList{{idx}}')" title="删除">&times;</span>{{/delBtn}}
							</td></shiro:hasPermission>
						</tr>//-->
					</script>
					<script type="text/javascript">
						var shopStockdetailRowIdx = 0, shopStockdetailTpl = $("#shopStockdetailTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
						$(document).ready(function() {
							var data = ${fns:toJson(shopStock.shopStockdetailList)};
							for (var i=0; i<data.length; i++){
								addRow('#shopStockdetailList', shopStockdetailRowIdx, shopStockdetailTpl, data[i]);
								shopStockdetailRowIdx = shopStockdetailRowIdx + 1;
							}
						});
					</script>
				</div>
			</div>
		<div class="form-actions">
			<shiro:hasPermission name="shop:shopStock:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>