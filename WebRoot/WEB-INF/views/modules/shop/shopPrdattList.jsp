<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>产品属性分类管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/shop/shopPrdatt/">产品属性分类列表</a></li>
		<shiro:hasPermission name="shop:shopPrdatt:edit"><li><a href="${ctx}/shop/shopPrdatt/form">产品属性分类添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="shopPrdatt" action="${ctx}/shop/shopPrdatt/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>名称：</label>
				<form:input path="attname" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>编号：</label>
				<form:input path="attnumber" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>备注：</label>
				<form:input path="remarks" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>名称</th>
				<th>编号</th>
				<th>update_date</th>
				<th>备注</th>
				<shiro:hasPermission name="shop:shopPrdatt:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="shopPrdatt">
			<tr>
				<td><a href="${ctx}/shop/shopPrdatt/form?id=${shopPrdatt.id}">
					${shopPrdatt.attname}
				</a></td>
				<td>
					${shopPrdatt.attnumber}
				</td>
				<td>
					<fmt:formatDate value="${shopPrdatt.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${shopPrdatt.remarks}
				</td>
				<shiro:hasPermission name="shop:shopPrdatt:edit"><td>
    				<a href="${ctx}/shop/shopPrdatt/form?id=${shopPrdatt.id}">修改</a>
					<a href="${ctx}/shop/shopPrdatt/delete?id=${shopPrdatt.id}" onclick="return confirmx('确认要删除该产品属性分类吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>