<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>仓库管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/shop/shopStorehouse/">仓库管理列表</a></li>
		<shiro:hasPermission name="shop:shopStorehouse:edit"><li><a href="${ctx}/shop/shopStorehouse/form">仓库管理添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="shopStorehouse" action="${ctx}/shop/shopStorehouse/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>仓库名称：</label>
				<form:input path="storename" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>仓库简称：</label>
				<form:input path="shortname" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>仓库编号：</label>
				<form:input path="storenumber" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>联系人：</label>
				<form:input path="person" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>电话：</label>
				<form:input path="telephone" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li><label>地址：</label>
				<form:input path="address" htmlEscape="false" maxlength="128" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>仓库名称</th>
				<th>仓库简称</th>
				<th>仓库编号</th>
				<th>联系人</th>
				<th>电话</th>
				<th>地址</th>
				<th>更新时间</th>
				<th>备注</th>
				<shiro:hasPermission name="shop:shopStorehouse:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="shopStorehouse">
			<tr>
				<td><a href="${ctx}/shop/shopStorehouse/form?id=${shopStorehouse.id}">
					${shopStorehouse.storename}
				</a></td>
				<td>
					${shopStorehouse.shortname}
				</td>
				<td>
					${shopStorehouse.storenumber}
				</td>
				<td>
					${shopStorehouse.person}
				</td>
				<td>
					${shopStorehouse.telephone}
				</td>
				<td>
					${shopStorehouse.address}
				</td>
				<td>
					<fmt:formatDate value="${shopStorehouse.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${shopStorehouse.remarks}
				</td>
				<shiro:hasPermission name="shop:shopStorehouse:edit"><td>
    				<a href="${ctx}/shop/shopStorehouse/form?id=${shopStorehouse.id}">修改</a>
					<a href="${ctx}/shop/shopStorehouse/delete?id=${shopStorehouse.id}" onclick="return confirmx('确认要删除该仓库管理吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>