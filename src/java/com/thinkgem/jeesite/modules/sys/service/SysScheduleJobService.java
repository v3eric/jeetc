/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.sys.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.sys.entity.SysScheduleJob;
import com.thinkgem.jeesite.modules.sys.dao.SysScheduleJobDao;

/**
 * 定时任务Service
 * @author 大胖老师
 * @version 2016-10-21
 */
@Service
@Transactional(readOnly = true)
public class SysScheduleJobService extends CrudService<SysScheduleJobDao, SysScheduleJob> {

	public SysScheduleJob get(String id) {
		return super.get(id);
	}
	
	public List<SysScheduleJob> findList(SysScheduleJob sysScheduleJob) {
		return super.findList(sysScheduleJob);
	}
	
	public Page<SysScheduleJob> findPage(Page<SysScheduleJob> page, SysScheduleJob sysScheduleJob) {
		return super.findPage(page, sysScheduleJob);
	}
	
	@Transactional(readOnly = false)
	public void save(SysScheduleJob sysScheduleJob) {
		super.save(sysScheduleJob);
	}
	
	@Transactional(readOnly = false)
	public void delete(SysScheduleJob sysScheduleJob) {
		super.delete(sysScheduleJob);
	}
	
}