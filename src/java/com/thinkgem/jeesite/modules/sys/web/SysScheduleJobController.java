/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.sys.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.sys.entity.SysScheduleJob;
import com.thinkgem.jeesite.modules.sys.service.SysScheduleJobService;

/**
 * 定时任务Controller
 * @author 大胖老师
 * @version 2016-10-21
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/sysScheduleJob")
public class SysScheduleJobController extends BaseController {

	@Autowired
	private SysScheduleJobService sysScheduleJobService;
	
	@ModelAttribute
	public SysScheduleJob get(@RequestParam(required=false) String id) {
		SysScheduleJob entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = sysScheduleJobService.get(id);
		}
		if (entity == null){
			entity = new SysScheduleJob();
		}
		return entity;
	}
	
	@RequiresPermissions("sys:sysScheduleJob:view")
	@RequestMapping(value = {"list", ""})
	public String list(SysScheduleJob sysScheduleJob, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SysScheduleJob> page = sysScheduleJobService.findPage(new Page<SysScheduleJob>(request, response), sysScheduleJob); 
		model.addAttribute("page", page);
		return "modules/sys/sysScheduleJobList";
	}

	@RequiresPermissions("sys:sysScheduleJob:view")
	@RequestMapping(value = "form")
	public String form(SysScheduleJob sysScheduleJob, Model model) {
		model.addAttribute("sysScheduleJob", sysScheduleJob);
		return "modules/sys/sysScheduleJobForm";
	}

	@RequiresPermissions("sys:sysScheduleJob:edit")
	@RequestMapping(value = "save")
	public String save(SysScheduleJob sysScheduleJob, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, sysScheduleJob)){
			return form(sysScheduleJob, model);
		}
		sysScheduleJobService.save(sysScheduleJob);
		addMessage(redirectAttributes, "保存定时任务成功");
		return "redirect:"+Global.getAdminPath()+"/sys/sysScheduleJob/?repage";
	}
	
	@RequiresPermissions("sys:sysScheduleJob:edit")
	@RequestMapping(value = "delete")
	public String delete(SysScheduleJob sysScheduleJob, RedirectAttributes redirectAttributes) {
		sysScheduleJobService.delete(sysScheduleJob);
		addMessage(redirectAttributes, "删除定时任务成功");
		return "redirect:"+Global.getAdminPath()+"/sys/sysScheduleJob/?repage";
	}

}