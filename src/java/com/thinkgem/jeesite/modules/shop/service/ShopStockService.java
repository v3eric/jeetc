/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopStock;
import com.thinkgem.jeesite.modules.shop.dao.ShopStockDao;
import com.thinkgem.jeesite.modules.shop.entity.ShopStockdetail;
import com.thinkgem.jeesite.modules.shop.dao.ShopStockdetailDao;

/**
 * 库存Service
 * @author water
 * @version 2016-01-15
 */
@Service
@Transactional(readOnly = true)
public class ShopStockService extends CrudService<ShopStockDao, ShopStock> {

	@Autowired
	private ShopStockdetailDao shopStockdetailDao;
	
	public ShopStock get(String id) {
		ShopStock shopStock = super.get(id);
		shopStock.setShopStockdetailList(shopStockdetailDao.findList(new ShopStockdetail(shopStock)));
		return shopStock;
	}
	
	public List<ShopStock> findList(ShopStock shopStock) {
		return super.findList(shopStock);
	}
	
	
	public List<ShopStockdetail> sumqty() {
		return shopStockdetailDao.sumqty();
	}
	
	public Page<ShopStock> findPage(Page<ShopStock> page, ShopStock shopStock) {
		return super.findPage(page, shopStock);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopStock shopStock) {
		super.save(shopStock);
		for (ShopStockdetail shopStockdetail : shopStock.getShopStockdetailList()){
			if (shopStockdetail.getId() == null){
				continue;
			}
			if (ShopStockdetail.DEL_FLAG_NORMAL.equals(shopStockdetail.getDelFlag())){
				if (StringUtils.isBlank(shopStockdetail.getId())){
					shopStockdetail.setShopStockId(shopStock);
					shopStockdetail.preInsert();
					shopStockdetailDao.insert(shopStockdetail);
				}else{
					shopStockdetail.preUpdate();
					shopStockdetailDao.update(shopStockdetail);
				}
			}else{
				shopStockdetailDao.delete(shopStockdetail);
			}
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopStock shopStock) {
		super.delete(shopStock);
		shopStockdetailDao.delete(new ShopStockdetail(shopStock));
	}
	
}