/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopGoods;
import com.thinkgem.jeesite.modules.shop.dao.ShopGoodsDao;
import com.thinkgem.jeesite.modules.shop.entity.ShopGoodetail;
import com.thinkgem.jeesite.modules.shop.dao.ShopGoodetailDao;

/**
 * 货品库存Service
 * @author water
 * @version 2015-12-16
 */
@Service
@Transactional(readOnly = true)
public class ShopGoodsService extends CrudService<ShopGoodsDao, ShopGoods> {

	@Autowired
	private ShopGoodetailDao shopGoodetailDao;
	
	public ShopGoods get(String id) {
		ShopGoods shopGoods = super.get(id);
		shopGoods.setShopGoodetailList(shopGoodetailDao.findList(new ShopGoodetail(shopGoods)));
		return shopGoods;
	}
	
	public List<ShopGoods> findList(ShopGoods shopGoods) {
		return super.findList(shopGoods);
	}
	
	public Page<ShopGoods> findPage(Page<ShopGoods> page, ShopGoods shopGoods) {
		return super.findPage(page, shopGoods);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopGoods shopGoods) {
		super.save(shopGoods);
		for (ShopGoodetail shopGoodetail : shopGoods.getShopGoodetailList()){
			if (shopGoodetail.getId() == null){
				continue;
			}
			if (ShopGoodetail.DEL_FLAG_NORMAL.equals(shopGoodetail.getDelFlag())){
				if (StringUtils.isBlank(shopGoodetail.getId())){
					shopGoodetail.setShopGoodsId(shopGoods);
					shopGoodetail.preInsert();
					shopGoodetailDao.insert(shopGoodetail);
				}else{
					shopGoodetail.preUpdate();
					shopGoodetailDao.update(shopGoodetail);
				}
			}else{
				shopGoodetailDao.delete(shopGoodetail);
			}
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopGoods shopGoods) {
		super.delete(shopGoods);
		shopGoodetailDao.delete(new ShopGoodetail(shopGoods));
	}
	
}