/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopBrand;
import com.thinkgem.jeesite.modules.shop.entity.ShopCategory;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.thinkgem.jeesite.modules.cms.dao.CategoryDao;
import com.thinkgem.jeesite.modules.cms.entity.Category;
import com.thinkgem.jeesite.modules.shop.dao.ShopBrandDao;
import com.thinkgem.jeesite.modules.shop.dao.ShopCategoryDao;

/**
 * 产品品牌Service
 * @author water
 * @version 2015-12-22
 */
@Service
@Transactional(readOnly = true)
public class ShopBrandService extends CrudService<ShopBrandDao, ShopBrand> {
	
	@Autowired
	private ShopCategoryDao shopCategoryDao;

	public ShopBrand get(String id) {
		return super.get(id);
	}
	
	public List<ShopBrand> findList(ShopBrand shopBrand) {
		return super.findList(shopBrand);
	}
	
	public Page<ShopBrand> findPage(Page<ShopBrand> page, ShopBrand shopBrand) {
		User user = UserUtils.getUser();
		shopBrand.getSqlMap().put("dsf", dataScopeFilter(user, "o", "u"));
		if (shopBrand.getShopCategory()!=null && StringUtils.isNotBlank(shopBrand.getShopCategory().getId())){
			ShopCategory shopCategory = shopCategoryDao.get(shopBrand.getShopCategory().getId());
			if (shopCategory==null){
				shopCategory = new ShopCategory();
			}
			shopBrand.setShopCategory(shopCategory);
		}
		else{
			shopBrand.setShopCategory(new ShopCategory() );
		}
	
		return super.findPage(page, shopBrand);
	}
	
	
	@Transactional(readOnly = false)
	public void save(ShopBrand shopBrand) {
		super.save(shopBrand);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopBrand shopBrand) {
		super.delete(shopBrand);
	}
	
}