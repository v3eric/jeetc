/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.shop.entity.ShopPrdmark;
import com.thinkgem.jeesite.modules.shop.dao.ShopPrdmarkDao;

/**
 * 产品特征码Service
 * @author water
 * @version 2016-01-13
 */
@Service
@Transactional(readOnly = true)
public class ShopPrdmarkService extends CrudService<ShopPrdmarkDao, ShopPrdmark> {

	public ShopPrdmark get(String id) {
		return super.get(id);
	}
	
	public List<ShopPrdmark> findList(ShopPrdmark shopPrdmark) {
		return super.findList(shopPrdmark);
	}
	
	public Page<ShopPrdmark> findPage(Page<ShopPrdmark> page, ShopPrdmark shopPrdmark) {
		return super.findPage(page, shopPrdmark);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopPrdmark shopPrdmark) {
		super.save(shopPrdmark);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopPrdmark shopPrdmark) {
		super.delete(shopPrdmark);
	}
	
}