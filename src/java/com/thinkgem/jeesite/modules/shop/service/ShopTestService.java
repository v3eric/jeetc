/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.shop.entity.ShopTest;
import com.thinkgem.jeesite.modules.shop.dao.ShopTestDao;

/**
 * 测试Service
 * @author water
 * @version 2016-02-23
 */
@Service
@Transactional(readOnly = true)
public class ShopTestService extends CrudService<ShopTestDao, ShopTest> {

	public ShopTest get(String id) {
		return super.get(id);
	}
	
	public List<ShopTest> findList(ShopTest shopTest) {
		return super.findList(shopTest);
	}
	
	public Page<ShopTest> findPage(Page<ShopTest> page, ShopTest shopTest) {
		return super.findPage(page, shopTest);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopTest shopTest) {
		super.save(shopTest);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopTest shopTest) {
		super.delete(shopTest);
	}
	
}