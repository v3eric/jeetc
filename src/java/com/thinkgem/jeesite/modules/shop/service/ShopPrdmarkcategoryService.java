/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.shop.entity.ShopPrdmarkcategory;
import com.thinkgem.jeesite.modules.shop.dao.ShopPrdmarkcategoryDao;

/**
 * 特征分类Service
 * @author water
 * @version 2016-09-06
 */
@Service
@Transactional(readOnly = true)
public class ShopPrdmarkcategoryService extends CrudService<ShopPrdmarkcategoryDao, ShopPrdmarkcategory> {

	public ShopPrdmarkcategory get(String id) {
		return super.get(id);
	}
	
	public List<ShopPrdmarkcategory> findList(ShopPrdmarkcategory shopPrdmarkcategory) {
		return super.findList(shopPrdmarkcategory);
	}
	
	public Page<ShopPrdmarkcategory> findPage(Page<ShopPrdmarkcategory> page, ShopPrdmarkcategory shopPrdmarkcategory) {
		return super.findPage(page, shopPrdmarkcategory);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopPrdmarkcategory shopPrdmarkcategory) {
		super.save(shopPrdmarkcategory);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopPrdmarkcategory shopPrdmarkcategory) {
		super.delete(shopPrdmarkcategory);
	}
	
}