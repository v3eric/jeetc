/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopAttribute;
import com.thinkgem.jeesite.modules.shop.dao.ShopAttributeDao;
import com.thinkgem.jeesite.modules.shop.entity.ShopAttrivalue;
import com.thinkgem.jeesite.modules.shop.entity.ShopCategory;
import com.thinkgem.jeesite.modules.shop.dao.ShopAttrivalueDao;
import com.thinkgem.jeesite.modules.shop.dao.ShopCategoryDao;

/**
 * 产品分类属性Service
 * @author jjf
 * @version 2015-12-29
 */
@Service
@Transactional(readOnly = true)
public class ShopAttributeService extends CrudService<ShopAttributeDao, ShopAttribute> {

	@Autowired
	private ShopAttrivalueDao shopAttrivalueDao;
	@Autowired
	private ShopCategoryDao shopCategoryDao;
	
	
	
	public ShopAttribute get(String id) {
		ShopAttribute shopAttribute = super.get(id);
		shopAttribute.setShopAttrivalueList(shopAttrivalueDao.findList(new ShopAttrivalue(shopAttribute)));
		return shopAttribute;
	}
	
	public List<ShopAttribute> findList(ShopAttribute shopAttribute) {
		return super.findList(shopAttribute);
	}
	
	public Page<ShopAttribute> findPage(Page<ShopAttribute> page, ShopAttribute shopAttribute) {
		

		if (shopAttribute.getShopCategory()!=null && StringUtils.isNotBlank(shopAttribute.getShopCategory().getId())){
			ShopCategory shopCategory = shopCategoryDao.get(shopAttribute.getShopCategory().getId());
			if (shopCategory==null){
				shopCategory = new ShopCategory();
			}
			shopAttribute.setShopCategory(shopCategory);
		}
		else{
			shopAttribute.setShopCategory(new ShopCategory() );
		}
	
		
		
		
		return super.findPage(page, shopAttribute);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopAttribute shopAttribute) {
		super.save(shopAttribute);
		for (ShopAttrivalue shopAttrivalue : shopAttribute.getShopAttrivalueList()){
			if (shopAttrivalue.getId() == null){
				continue;
			}
			if (ShopAttrivalue.DEL_FLAG_NORMAL.equals(shopAttrivalue.getDelFlag())){
				if (StringUtils.isBlank(shopAttrivalue.getId())){
					shopAttrivalue.setShopAttribute(shopAttribute);
					shopAttrivalue.preInsert();
					shopAttrivalueDao.insert(shopAttrivalue);
				}else{
					shopAttrivalue.preUpdate();
					shopAttrivalueDao.update(shopAttrivalue);
				}
			}else{
				shopAttrivalueDao.delete(shopAttrivalue);
			}
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopAttribute shopAttribute) {
		super.delete(shopAttribute);
		shopAttrivalueDao.delete(new ShopAttrivalue(shopAttribute));
	}
	
}