/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.shop.entity.ShopStorehouse;
import com.thinkgem.jeesite.modules.shop.dao.ShopStorehouseDao;

/**
 * 仓库管理Service
 * @author water
 * @version 2016-09-06
 */
@Service
@Transactional(readOnly = true)
public class ShopStorehouseService extends CrudService<ShopStorehouseDao, ShopStorehouse> {

	public ShopStorehouse get(String id) {
		return super.get(id);
	}
	
	public List<ShopStorehouse> findList(ShopStorehouse shopStorehouse) {
		return super.findList(shopStorehouse);
	}
	
	public Page<ShopStorehouse> findPage(Page<ShopStorehouse> page, ShopStorehouse shopStorehouse) {
		return super.findPage(page, shopStorehouse);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopStorehouse shopStorehouse) {
		super.save(shopStorehouse);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopStorehouse shopStorehouse) {
		super.delete(shopStorehouse);
	}
	
}