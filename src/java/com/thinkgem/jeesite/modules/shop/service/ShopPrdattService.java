/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.shop.entity.ShopPrdatt;
import com.thinkgem.jeesite.modules.shop.dao.ShopPrdattDao;

/**
 * 产品属性分类Service
 * @author water
 * @version 2016-09-06
 */
@Service
@Transactional(readOnly = true)
public class ShopPrdattService extends CrudService<ShopPrdattDao, ShopPrdatt> {

	public ShopPrdatt get(String id) {
		return super.get(id);
	}
	
	public List<ShopPrdatt> findList(ShopPrdatt shopPrdatt) {
		return super.findList(shopPrdatt);
	}
	
	public Page<ShopPrdatt> findPage(Page<ShopPrdatt> page, ShopPrdatt shopPrdatt) {
		return super.findPage(page, shopPrdatt);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopPrdatt shopPrdatt) {
		super.save(shopPrdatt);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopPrdatt shopPrdatt) {
		super.delete(shopPrdatt);
	}
	
}