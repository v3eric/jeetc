/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.shop.entity.ShopProduct;
import com.thinkgem.jeesite.modules.shop.dao.ShopProductDao;

/**
 * 商品资料Service
 * @author water
 * @version 2016-01-13
 */
@Service
@Transactional(readOnly = true)
public class ShopProductService extends CrudService<ShopProductDao, ShopProduct> {

	public ShopProduct get(String id) {
		return super.get(id);
	}
	
	public List<ShopProduct> findList(ShopProduct shopProduct) {
		return super.findList(shopProduct);
	}
	
	public Page<ShopProduct> findPage(Page<ShopProduct> page, ShopProduct shopProduct) {
		return super.findPage(page, shopProduct);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopProduct shopProduct) {
		super.save(shopProduct);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopProduct shopProduct) {
		super.delete(shopProduct);
	}
	
}