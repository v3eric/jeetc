/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.shop.entity.ShopCustsup;
import com.thinkgem.jeesite.modules.shop.dao.ShopCustsupDao;

/**
 * 客户厂商Service
 * @author water
 * @version 2016-01-14
 */
@Service
@Transactional(readOnly = true)
public class ShopCustsupService extends CrudService<ShopCustsupDao, ShopCustsup> {

	public ShopCustsup get(String id) {
		return super.get(id);
	}
	
	public List<ShopCustsup> findList(ShopCustsup shopCustsup) {
		return super.findList(shopCustsup);
	}
	
	public Page<ShopCustsup> findPage(Page<ShopCustsup> page, ShopCustsup shopCustsup) {
		return super.findPage(page, shopCustsup);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopCustsup shopCustsup) {
		super.save(shopCustsup);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopCustsup shopCustsup) {
		super.delete(shopCustsup);
	}
	
}