/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.shop.entity.ShopStorehouse;

/**
 * 仓库管理DAO接口
 * @author water
 * @version 2016-09-06
 */
@MyBatisDao
public interface ShopStorehouseDao extends CrudDao<ShopStorehouse> {
	
}