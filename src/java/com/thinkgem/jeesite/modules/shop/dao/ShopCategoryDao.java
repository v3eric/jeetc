/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.dao;

import com.thinkgem.jeesite.common.persistence.TreeDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.shop.entity.ShopCategory;

/**
 * 产品分类DAO接口
 * @author water
 * @version 2015-12-22
 */
@MyBatisDao
public interface ShopCategoryDao extends TreeDao<ShopCategory> {
	
}