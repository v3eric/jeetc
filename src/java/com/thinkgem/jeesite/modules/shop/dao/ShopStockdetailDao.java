/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.dao;

import java.util.List;


import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.shop.entity.ShopStockdetail;

/**
 * 库存DAO接口
 * @author water
 * @version 2016-01-15
 */
@MyBatisDao
public interface ShopStockdetailDao extends CrudDao<ShopStockdetail> {
	
	public List<ShopStockdetail> sumqty();
	
}