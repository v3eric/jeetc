/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.shop.entity.ShopAttrivalue;

/**
 * 产品分类属性DAO接口
 * @author jjf
 * @version 2015-12-29
 */
@MyBatisDao
public interface ShopAttrivalueDao extends CrudDao<ShopAttrivalue> {
	
}