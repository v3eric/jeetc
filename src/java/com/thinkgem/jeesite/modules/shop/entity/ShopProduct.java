/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 商品资料Entity
 * @author water
 * @version 2016-01-13
 */
public class ShopProduct extends DataEntity<ShopProduct> {
	
	private static final long serialVersionUID = 1L;
	private String pname;		// 名称
	private ShopBrand shopBrand;		// 所属品牌
	private ShopCategory shopCategory;		// 所属分类
	private String price;		// 列表价格
	private String focus;		// 宝贝卖点
	private String qty;		// 数量
	private String productnum;		// 商家编码
	private String ongoodstype;		// 上架类型.-定时上架-按照时间
	private Date ongoodstypetime;		// 上架时间
	private String volume;		// 体积
	private String weight;		// 重量
	private String sysAreaId;		// 宝贝所在地区
	private String expressId;		// 快递模版
	private ShopPrdatt shopPrdatt;		// 属性分类
	private ShopPrdmarkcategory shopPrdmarkcategory;		// 特征模版
	
	public ShopPrdatt getShopPrdatt() {
		return shopPrdatt;
	}

	public void setShopPrdatt(ShopPrdatt shopPrdatt) {
		this.shopPrdatt = shopPrdatt;
	}

	public ShopPrdmarkcategory getShopPrdmarkcategory() {
		return shopPrdmarkcategory;
	}

	public void setShopPrdmarkcategory(ShopPrdmarkcategory shopPrdmarkcategory) {
		this.shopPrdmarkcategory = shopPrdmarkcategory;
	}
	
	public ShopProduct() {
		super();
	}

	public ShopProduct(String id){
		super(id);
	}

	@Length(min=0, max=128, message="名称长度必须介于 0 和 128 之间")
	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}
	
	
	
	public ShopBrand getShopBrand() {
		return shopBrand;
	}

	public void setShopBrand(ShopBrand shopBrand) {
		this.shopBrand = shopBrand;
	}

	public ShopCategory getShopCategory() {
		return shopCategory;
	}

	public void setShopCategory(ShopCategory shopCategory) {
		this.shopCategory = shopCategory;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@Length(min=0, max=512, message="宝贝卖点长度必须介于 0 和 512 之间")
	public String getFocus() {
		return focus;
	}

	public void setFocus(String focus) {
		this.focus = focus;
	}
	
	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}
	
	@Length(min=0, max=64, message="商家编码长度必须介于 0 和 64 之间")
	public String getProductnum() {
		return productnum;
	}

	public void setProductnum(String productnum) {
		this.productnum = productnum;
	}
	
	
	public String getOngoodstype() {
		return ongoodstype;
	}

	public void setOngoodstype(String ongoodstype) {
		this.ongoodstype = ongoodstype;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getOngoodstypetime() {
		return ongoodstypetime;
	}

	public void setOngoodstypetime(Date ongoodstypetime) {
		this.ongoodstypetime = ongoodstypetime;
	}
	
	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	@Length(min=0, max=64, message="宝贝所在地区长度必须介于 0 和 64 之间")
	public String getSysAreaId() {
		return sysAreaId;
	}

	public void setSysAreaId(String sysAreaId) {
		this.sysAreaId = sysAreaId;
	}
	
	@Length(min=0, max=64, message="快递模版长度必须介于 0 和 64 之间")
	public String getExpressId() {
		return expressId;
	}

	public void setExpressId(String expressId) {
		this.expressId = expressId;
	}
	
	
	
	
}