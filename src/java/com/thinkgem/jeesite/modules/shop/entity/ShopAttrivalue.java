/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 产品分类属性Entity
 * @author jjf
 * @version 2015-12-29
 */
public class ShopAttrivalue extends DataEntity<ShopAttrivalue> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 属性值中间用-分割
	private ShopAttribute shopAttribute;		// 所属的属性 父类
	private String state;		// 状态
	private String sort;		// 排序
	
	public ShopAttrivalue() {
		super();
	}

	public ShopAttrivalue(String id){
		super(id);
	}

	public ShopAttrivalue(ShopAttribute shopAttribute){
		this.shopAttribute = shopAttribute;
	}

	@Length(min=0, max=256, message="属性值中间用-分割长度必须介于 0 和 256 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=64, message="所属的属性长度必须介于 0 和 64 之间")
	public ShopAttribute getShopAttribute() {
		return shopAttribute;
	}

	public void setShopAttribute(ShopAttribute shopAttribute) {
		this.shopAttribute = shopAttribute;
	}
	
	@Length(min=0, max=255, message="状态长度必须介于 0 和 255 之间")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@Length(min=0, max=64, message="排序长度必须介于 0 和 64 之间")
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
	
}