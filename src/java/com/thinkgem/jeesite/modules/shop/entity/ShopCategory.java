/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.TreeEntity;

/**
 * 产品分类Entity
 * @author water
 * @version 2015-12-22
 */
public class ShopCategory extends TreeEntity<ShopCategory> {
	
	private static final long serialVersionUID = 1L;
	private ShopCategory parent;		// 父类id
	private String parentIds;		// 所有父类ids
	private String name;		// 分类名称
	private Integer sort;		// 排序
	
	public ShopCategory() {
		super();
	}

	public ShopCategory(String id){
		super(id);
	}

	@JsonBackReference
	public ShopCategory getParent() {
		return parent;
	}

	public void setParent(ShopCategory parent) {
		this.parent = parent;
	}
	
	@Length(min=0, max=2000, message="所有父类ids长度必须介于 0 和 2000 之间")
	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}
	
	@Length(min=0, max=100, message="分类名称长度必须介于 0 和 100 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	public String getParentId() {
		return parent != null && parent.getId() != null ? parent.getId() : "0";
	}
}