/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import org.hibernate.validator.constraints.Length;
import java.util.List;
import com.google.common.collect.Lists;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 产品分类属性Entity
 * @author jjf
 * @version 2015-12-29
 */
public class ShopAttribute extends DataEntity<ShopAttribute> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 属性名称
	private ShopCategory shopCategory;		// 所属分类
	private String inputtype;		// 输入类型
	private ShopPrdatt shopPrdatt;		// 属性类别

	private String issearch;		// 是否搜索
	private String attcategory;  //属性分类
	private String state;		// 状态
	private String sort;		// 排序
	private List<ShopAttrivalue> shopAttrivalueList = Lists.newArrayList();		// 子表列表
	
	
	public ShopPrdatt getShopPrdatt() {
		return shopPrdatt;
	}

	public void setShopPrdatt(ShopPrdatt shopPrdatt) {
		this.shopPrdatt = shopPrdatt;
	}
	
	
	public ShopAttribute() {
		super();
	}

	public ShopAttribute(String id){
		super(id);
	}

	@Length(min=0, max=128, message="属性名称长度必须介于 0 和 128 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	public ShopCategory getShopCategory() {
		return shopCategory;
	}

	public void setShopCategory(ShopCategory shopCategory) {
		this.shopCategory = shopCategory;
	}

	@Length(min=0, max=64, message="输入类型长度必须介于 0 和 64 之间")
	public String getInputtype() {
		return inputtype;
	}

	public void setInputtype(String inputtype) {
		this.inputtype = inputtype;
	}
	
	
	
	@Length(min=0, max=64, message="是否搜索长度必须介于 0 和 64 之间")
	public String getIssearch() {
		return issearch;
	}

	public void setIssearch(String issearch) {
		this.issearch = issearch;
	}
	
	@Length(min=0, max=64, message="状态长度必须介于 0 和 64 之间")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@Length(min=0, max=64, message="排序长度必须介于 0 和 64 之间")
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
	
	public String getAttcategory() {
		return attcategory;
	}

	public void setAttcategory(String attcategory) {
		this.attcategory = attcategory;
	}

	
	public List<ShopAttrivalue> getShopAttrivalueList() {
		return shopAttrivalueList;
	}

	public void setShopAttrivalueList(List<ShopAttrivalue> shopAttrivalueList) {
		this.shopAttrivalueList = shopAttrivalueList;
	}
}