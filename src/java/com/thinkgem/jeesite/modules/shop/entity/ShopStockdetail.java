/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 库存Entity
 * @author water
 * @version 2016-01-15
 */
public class ShopStockdetail extends DataEntity<ShopStockdetail> {
	
	private static final long serialVersionUID = 1L;
	private ShopStock shopStockId;		// 出入库主表ID 父类
	private ShopProduct shopProduct;		// 货品编号
	public ShopProduct getShopProduct() {
		return shopProduct;
	}

	public void setShopProduct(ShopProduct shopProduct) {
		this.shopProduct = shopProduct;
	}

	public ShopPrdmark getShopPrdmark() {
		return shopPrdmark;
	}

	public void setShopPrdmark(ShopPrdmark shopPrdmark) {
		this.shopPrdmark = shopPrdmark;
	}

	private String name;		// 货品名称
	private ShopPrdmark shopPrdmark;		// 特征码
	private String qty;		// 数量
	private String price;		// 单价
	private String salprice;		// 小计算
	private String rem;		// 备注
	
	public ShopStockdetail() {
		super();
	}

	public ShopStockdetail(String id){
		super(id);
	}

	public ShopStockdetail(ShopStock shopStockId){
		this.shopStockId = shopStockId;
	}

	@Length(min=0, max=64, message="出入库主表ID长度必须介于 0 和 64 之间")
	public ShopStock getShopStockId() {
		return shopStockId;
	}

	public void setShopStockId(ShopStock shopStockId) {
		this.shopStockId = shopStockId;
	}
	
	
	
	@Length(min=0, max=255, message="货品名称长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	@Length(min=0, max=10, message="数量长度必须介于 0 和 10 之间")
	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}
	
	@Length(min=0, max=10, message="单价长度必须介于 0 和 10 之间")
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@Length(min=0, max=10, message="小计算长度必须介于 0 和 10 之间")
	public String getSalprice() {
		return salprice;
	}

	public void setSalprice(String salprice) {
		this.salprice = salprice;
	}
	
	@Length(min=0, max=255, message="备注长度必须介于 0 和 255 之间")
	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}
	
}