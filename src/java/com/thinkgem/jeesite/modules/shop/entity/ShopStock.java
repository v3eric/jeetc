/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Length;
import java.util.List;
import com.google.common.collect.Lists;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 库存Entity
 * @author water
 * @version 2016-01-15
 */
public class ShopStock extends DataEntity<ShopStock> {
	
	private static final long serialVersionUID = 1L;
	private Date jhdate;		// 进货日期
	private String rem;		// 备注
	private ShopCustsup custsupp;		// 进货厂商
	private String person;		// 业务人员
	private String cktype;		// 单据类别
	private String number;		// 进货单号
	private Date beginJhdate;		// 开始 进货日期
	private Date endJhdate;		// 结束 进货日期
	private List<ShopStockdetail> shopStockdetailList = Lists.newArrayList();		// 子表列表
	
	public ShopStock() {
		super();
	}

	public ShopStock(String id){
		super(id);
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getJhdate() {
		return jhdate;
	}

	public void setJhdate(Date jhdate) {
		this.jhdate = jhdate;
	}
	
	@Length(min=0, max=255, message="备注长度必须介于 0 和 255 之间")
	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}
	
	
	public ShopCustsup getCustsupp() {
		return custsupp;
	}

	public void setCustsupp(ShopCustsup custsupp) {
		this.custsupp = custsupp;
	}

	@Length(min=0, max=64, message="业务人员长度必须介于 0 和 64 之间")
	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}
	
	@Length(min=0, max=64, message="单据类别长度必须介于 0 和 64 之间")
	public String getCktype() {
		return cktype;
	}

	public void setCktype(String cktype) {
		this.cktype = cktype;
	}
	
	@Length(min=0, max=255, message="进货单号长度必须介于 0 和 255 之间")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	public Date getBeginJhdate() {
		return beginJhdate;
	}

	public void setBeginJhdate(Date beginJhdate) {
		this.beginJhdate = beginJhdate;
	}
	
	public Date getEndJhdate() {
		return endJhdate;
	}

	public void setEndJhdate(Date endJhdate) {
		this.endJhdate = endJhdate;
	}
		
	public List<ShopStockdetail> getShopStockdetailList() {
		return shopStockdetailList;
	}

	public void setShopStockdetailList(List<ShopStockdetail> shopStockdetailList) {
		this.shopStockdetailList = shopStockdetailList;
	}
}