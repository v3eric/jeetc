/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 产品属性分类Entity
 * @author water
 * @version 2016-09-06
 */
public class ShopPrdatt extends DataEntity<ShopPrdatt> {
	
	private static final long serialVersionUID = 1L;
	private String attname;		// 名称
	private String attnumber;		// 编号
	
	public ShopPrdatt() {
		super();
	}

	public ShopPrdatt(String id){
		super(id);
	}

	@Length(min=0, max=64, message="名称长度必须介于 0 和 64 之间")
	public String getAttname() {
		return attname;
	}

	public void setAttname(String attname) {
		this.attname = attname;
	}
	
	@Length(min=0, max=64, message="编号长度必须介于 0 和 64 之间")
	public String getAttnumber() {
		return attnumber;
	}

	public void setAttnumber(String attnumber) {
		this.attnumber = attnumber;
	}
	
}