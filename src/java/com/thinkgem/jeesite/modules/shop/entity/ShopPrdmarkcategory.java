/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 特征分类Entity
 * @author water
 * @version 2016-09-06
 */
public class ShopPrdmarkcategory extends DataEntity<ShopPrdmarkcategory> {
	
	private static final long serialVersionUID = 1L;
	private String pname;		// 分类名称
	private String pnumber;		// 分类编号
	
	public ShopPrdmarkcategory() {
		super();
	}

	public ShopPrdmarkcategory(String id){
		super(id);
	}

	@Length(min=0, max=255, message="分类名称长度必须介于 0 和 255 之间")
	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}
	
	@Length(min=0, max=255, message="分类编号长度必须介于 0 和 255 之间")
	public String getPnumber() {
		return pnumber;
	}

	public void setPnumber(String pnumber) {
		this.pnumber = pnumber;
	}
	
}