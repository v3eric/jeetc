/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 仓库管理Entity
 * @author water
 * @version 2016-09-06
 */
public class ShopStorehouse extends DataEntity<ShopStorehouse> {
	
	private static final long serialVersionUID = 1L;
	private String storename;		// 仓库名称
	private String storenumber;		// 仓库编号
	private String person;		// 联系人
	private String telephone;		// 电话
	private String address;		// 地址
	private String zipcode;		// 邮编
	private String shortname;   //仓库简称
	
	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public ShopStorehouse() {
		super();
	}

	public ShopStorehouse(String id){
		super(id);
	}

	@Length(min=0, max=128, message="仓库名称长度必须介于 0 和 128 之间")
	public String getStorename() {
		return storename;
	}

	public void setStorename(String storename) {
		this.storename = storename;
	}
	
	@Length(min=0, max=128, message="仓库编号长度必须介于 0 和 128 之间")
	public String getStorenumber() {
		return storenumber;
	}

	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}
	
	@Length(min=0, max=128, message="联系人长度必须介于 0 和 128 之间")
	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}
	
	@Length(min=0, max=128, message="电话长度必须介于 0 和 128 之间")
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	@Length(min=0, max=128, message="地址长度必须介于 0 和 128 之间")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Length(min=0, max=128, message="邮编长度必须介于 0 和 128 之间")
	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
}