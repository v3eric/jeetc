/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 产品特征码Entity
 * @author water
 * @version 2016-01-13
 */
public class ShopPrdmark extends DataEntity<ShopPrdmark> {
	
	private static final long serialVersionUID = 1L;
	private String markname;		// 特征名称
	private String marknumber;		// 特征编码
	private String categorynumber;		// 分类编码
	private ShopPrdmarkcategory shopPrdmarkcategory;		// 分类名称

	
	public ShopPrdmarkcategory getShopPrdmarkcategory() {
		return shopPrdmarkcategory;
	}

	public void setShopPrdmarkcategory(ShopPrdmarkcategory shopPrdmarkcategory) {
		this.shopPrdmarkcategory = shopPrdmarkcategory;
	}
	
	public ShopPrdmark() {
		super();
	}

	public ShopPrdmark(String id){
		super(id);
	}

	@Length(min=0, max=128, message="特征名称长度必须介于 0 和 128 之间")
	public String getMarkname() {
		return markname;
	}

	public void setMarkname(String markname) {
		this.markname = markname;
	}
	
	@Length(min=0, max=128, message="特征编码长度必须介于 0 和 128 之间")
	public String getMarknumber() {
		return marknumber;
	}

	public void setMarknumber(String marknumber) {
		this.marknumber = marknumber;
	}
	
	@Length(min=0, max=128, message="分类编码长度必须介于 0 和 128 之间")
	public String getCategorynumber() {
		return categorynumber;
	}

	public void setCategorynumber(String categorynumber) {
		this.categorynumber = categorynumber;
	}
	
	
	
}