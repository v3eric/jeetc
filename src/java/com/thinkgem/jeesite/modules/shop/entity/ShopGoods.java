/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import org.hibernate.validator.constraints.Length;
import java.util.List;
import com.google.common.collect.Lists;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 货品库存Entity
 * @author water
 * @version 2015-12-16
 */
public class ShopGoods extends DataEntity<ShopGoods> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 名称
	private String rem;		// 备注
	private String price;		// 价格
	private String qty;		// 数量
	private String number;		// 编号
	private String picture;		// 图片
	private List<ShopGoodetail> shopGoodetailList = Lists.newArrayList();		// 子表列表
	
	public ShopGoods() {
		super();
	}

	public ShopGoods(String id){
		super(id);
	}

	@Length(min=0, max=255, message="名称长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=255, message="备注长度必须介于 0 和 255 之间")
	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}
	
	@Length(min=0, max=64, message="价格长度必须介于 0 和 64 之间")
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@Length(min=0, max=64, message="数量长度必须介于 0 和 64 之间")
	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}
	
	@Length(min=0, max=64, message="编号长度必须介于 0 和 64 之间")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	@Length(min=0, max=255, message="图片长度必须介于 0 和 255 之间")
	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	public List<ShopGoodetail> getShopGoodetailList() {
		return shopGoodetailList;
	}

	public void setShopGoodetailList(List<ShopGoodetail> shopGoodetailList) {
		this.shopGoodetailList = shopGoodetailList;
	}
}