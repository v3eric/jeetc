/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 货品库存Entity
 * @author water
 * @version 2015-12-16
 */
public class ShopGoodetail extends DataEntity<ShopGoodetail> {
	
	private static final long serialVersionUID = 1L;
	private ShopGoods shopGoodsId;		// 父表ID 父类
	private String name;		// 名称
	private String number;		// 编号
	private String qty;		// 销货数量
	private String price;		// 价格
	private String salprice;		// 销货价格
	private String rem;		// 备注
	
	public ShopGoodetail() {
		super();
	}

	public ShopGoodetail(String id){
		super(id);
	}

	public ShopGoodetail(ShopGoods shopGoodsId){
		this.shopGoodsId = shopGoodsId;
	}

	@Length(min=0, max=64, message="父表ID长度必须介于 0 和 64 之间")
	public ShopGoods getShopGoodsId() {
		return shopGoodsId;
	}

	public void setShopGoodsId(ShopGoods shopGoodsId) {
		this.shopGoodsId = shopGoodsId;
	}
	
	@Length(min=0, max=255, message="名称长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=255, message="编号长度必须介于 0 和 255 之间")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	@Length(min=0, max=10, message="销货数量长度必须介于 0 和 10 之间")
	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}
	
	@Length(min=0, max=10, message="价格长度必须介于 0 和 10 之间")
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@Length(min=0, max=10, message="销货价格长度必须介于 0 和 10 之间")
	public String getSalprice() {
		return salprice;
	}

	public void setSalprice(String salprice) {
		this.salprice = salprice;
	}
	
	@Length(min=0, max=255, message="备注长度必须介于 0 和 255 之间")
	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}
	
}