/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopBrand;
import com.thinkgem.jeesite.modules.shop.entity.ShopPrdmark;
import com.thinkgem.jeesite.modules.shop.entity.ShopPrdmarkcategory;
import com.thinkgem.jeesite.modules.shop.service.ShopPrdmarkService;
import com.thinkgem.jeesite.modules.shop.service.ShopPrdmarkcategoryService;

/**
 * 产品特征码Controller
 * @author water
 * @version 2016-01-13
 */
@Controller
@RequestMapping(value = "${adminPath}/shop/shopPrdmark")
public class ShopPrdmarkController extends BaseController {

	@Autowired
	private ShopPrdmarkService shopPrdmarkService;
	
	@Autowired
	private ShopPrdmarkcategoryService shopPrdmarkcategoryService;
	
	@ModelAttribute
	public ShopPrdmark get(@RequestParam(required=false) String id) {
		ShopPrdmark entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopPrdmarkService.get(id);
		}
		if (entity == null){
			entity = new ShopPrdmark();
		}
		return entity;
	}
	
	@RequiresPermissions("shop:shopPrdmark:view")
	@RequestMapping(value = {"list", ""})
	public String list(ShopPrdmark shopPrdmark, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopPrdmark> page = shopPrdmarkService.findPage(new Page<ShopPrdmark>(request, response), shopPrdmark); 
		model.addAttribute("page", page);
		return "modules/shop/shopPrdmarkList";
	}

	@RequiresPermissions("shop:shopPrdmark:view")
	@RequestMapping(value = "form")
	public String form(ShopPrdmark shopPrdmark, Model model) {
		
		model.addAttribute("shopPrdmarkCategoryList", shopPrdmarkcategoryService.findList(new ShopPrdmarkcategory()));
		
		
		model.addAttribute("shopPrdmark", shopPrdmark);
		return "modules/shop/shopPrdmarkForm";
	}

	@RequiresPermissions("shop:shopPrdmark:edit")
	@RequestMapping(value = "save")
	public String save(ShopPrdmark shopPrdmark, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, shopPrdmark)){
			return form(shopPrdmark, model);
		}
		shopPrdmarkService.save(shopPrdmark);
		addMessage(redirectAttributes, "保存产品特征码成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopPrdmark/?repage";
	}
	
	@RequiresPermissions("shop:shopPrdmark:edit")
	@RequestMapping(value = "delete")
	public String delete(ShopPrdmark shopPrdmark, RedirectAttributes redirectAttributes) {
		shopPrdmarkService.delete(shopPrdmark);
		addMessage(redirectAttributes, "删除产品特征码成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopPrdmark/?repage";
	}

}