/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopPrdatt;
import com.thinkgem.jeesite.modules.shop.service.ShopPrdattService;
import com.thinkgem.jeesite.modules.shop.service.ShopProductService;

/**
 * 产品属性分类Controller
 * @author water
 * @version 2016-09-06
 */
@Controller
@RequestMapping(value = "${adminPath}/shop/shopPrdatt")
public class ShopPrdattController extends BaseController {

	@Autowired
	private ShopPrdattService shopPrdattService;
	
	
	
	@ModelAttribute
	public ShopPrdatt get(@RequestParam(required=false) String id) {
		ShopPrdatt entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopPrdattService.get(id);
		}
		if (entity == null){
			entity = new ShopPrdatt();
		}
		return entity;
	}
	
	@RequiresPermissions("shop:shopPrdatt:view")
	@RequestMapping(value = {"list", ""})
	public String list(ShopPrdatt shopPrdatt, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopPrdatt> page = shopPrdattService.findPage(new Page<ShopPrdatt>(request, response), shopPrdatt); 
		model.addAttribute("page", page);
		return "modules/shop/shopPrdattList";
	}

	@RequiresPermissions("shop:shopPrdatt:view")
	@RequestMapping(value = "form")
	public String form(ShopPrdatt shopPrdatt, Model model) {
		
		
		model.addAttribute("shopPrdatt", shopPrdatt);
		return "modules/shop/shopPrdattForm";
	}

	@RequiresPermissions("shop:shopPrdatt:edit")
	@RequestMapping(value = "save")
	public String save(ShopPrdatt shopPrdatt, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, shopPrdatt)){
			return form(shopPrdatt, model);
		}
		shopPrdattService.save(shopPrdatt);
		addMessage(redirectAttributes, "保存产品属性分类成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopPrdatt/?repage";
	}
	
	@RequiresPermissions("shop:shopPrdatt:edit")
	@RequestMapping(value = "delete")
	public String delete(ShopPrdatt shopPrdatt, RedirectAttributes redirectAttributes) {
		shopPrdattService.delete(shopPrdatt);
		addMessage(redirectAttributes, "删除产品属性分类成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopPrdatt/?repage";
	}

}