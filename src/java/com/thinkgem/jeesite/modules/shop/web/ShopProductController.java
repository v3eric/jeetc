/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopBrand;
import com.thinkgem.jeesite.modules.shop.entity.ShopCategory;
import com.thinkgem.jeesite.modules.shop.entity.ShopGoods;
import com.thinkgem.jeesite.modules.shop.entity.ShopPrdatt;
import com.thinkgem.jeesite.modules.shop.entity.ShopPrdmarkcategory;
import com.thinkgem.jeesite.modules.shop.entity.ShopProduct;
import com.thinkgem.jeesite.modules.shop.service.ShopBrandService;
import com.thinkgem.jeesite.modules.shop.service.ShopCategoryService;
import com.thinkgem.jeesite.modules.shop.service.ShopPrdattService;
import com.thinkgem.jeesite.modules.shop.service.ShopPrdmarkcategoryService;
import com.thinkgem.jeesite.modules.shop.service.ShopProductService;

/**
 * 商品资料Controller
 * @author water
 * @version 2016-01-13
 */
@Controller
@RequestMapping(value = "${adminPath}/shop/shopProduct")
public class ShopProductController extends BaseController {

	@Autowired
	private ShopProductService shopProductService;
	@Autowired
	private ShopBrandService shopBrandService;
	@Autowired
	private ShopCategoryService shopCategoryService;
	@Autowired
	private ShopPrdattService shopPrdattService;
	@Autowired
	private ShopPrdmarkcategoryService shopPrdmarkcategoryService;
	
	@ModelAttribute
	public ShopProduct get(@RequestParam(required=false) String id) {
		ShopProduct entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopProductService.get(id);
		}
		if (entity == null){
			entity = new ShopProduct();
		}
		return entity;
	}
	
	
	
	@RequiresPermissions("shop:shopProduct:view")
	@RequestMapping(value = {"list", ""})
	public String list(ShopProduct shopProduct, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopProduct> page = shopProductService.findPage(new Page<ShopProduct>(request, response), shopProduct); 
		model.addAttribute("shopBrandList", shopBrandService.findList(new ShopBrand()));
		model.addAttribute("page", page);
		return "modules/shop/shopProductList";
	}
	
	@RequiresPermissions("shop:shopProduct:view")
	@RequestMapping(value = "listdata")
	public String listData(ShopProduct shopProduct, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopProduct> page = shopProductService.findPage(new Page<ShopProduct>(request, response), shopProduct); 
		model.addAttribute("shopBrandList", shopBrandService.findList(new ShopBrand()));
		model.addAttribute("page", page);
		return "modules/shop/shopProductListData";
	}

	
	@RequiresPermissions("shop:shopProduct:view")
	@RequestMapping(value = "form")
	public String form(ShopProduct shopProduct,ShopBrand shopBrand, Model model) {
	
		if(shopProduct.getShopCategory()!=null){
			if (shopProduct.getShopCategory().getId()!=null && StringUtils.isNotBlank(shopProduct.getShopCategory().getId())){
				shopProduct.setShopCategory(shopCategoryService.get(shopProduct.getShopCategory().getId()));
			}
		}
		
		model.addAttribute("shopBrandList", shopBrandService.findList(new ShopBrand()));
		model.addAttribute("shopPrdattList", shopPrdattService.findList(new ShopPrdatt()));
		model.addAttribute("shopPrdmarkCategoryList", shopPrdmarkcategoryService.findList(new ShopPrdmarkcategory()));
//		model.addAttribute("shopCategory", shopCategory);
		model.addAttribute("shopProduct", shopProduct);
		return "modules/shop/shopProductForm";
	}

	@RequiresPermissions("shop:shopProduct:edit")
	@RequestMapping(value = "save")
	public String save(ShopProduct shopProduct, ShopBrand shopBrand,ShopCategory shopCategory,Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, shopProduct)){
			return form(shopProduct,shopBrand, model);
		}
		shopProductService.save(shopProduct);
		addMessage(redirectAttributes, "保存商品资料成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopProduct/?repage";
	}
	
	@RequiresPermissions("shop:shopProduct:edit")
	@RequestMapping(value = "delete")
	public String delete(ShopProduct shopProduct, RedirectAttributes redirectAttributes) {
		shopProductService.delete(shopProduct);
		addMessage(redirectAttributes, "删除商品资料成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopProduct/?repage";
	}

}