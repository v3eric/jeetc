/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopStorehouse;
import com.thinkgem.jeesite.modules.shop.service.ShopStorehouseService;

/**
 * 仓库管理Controller
 * @author water
 * @version 2016-09-06
 */
@Controller
@RequestMapping(value = "${adminPath}/shop/shopStorehouse")
public class ShopStorehouseController extends BaseController {

	@Autowired
	private ShopStorehouseService shopStorehouseService;
	
	@ModelAttribute
	public ShopStorehouse get(@RequestParam(required=false) String id) {
		ShopStorehouse entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopStorehouseService.get(id);
		}
		if (entity == null){
			entity = new ShopStorehouse();
		}
		return entity;
	}
	
	@RequiresPermissions("shop:shopStorehouse:view")
	@RequestMapping(value = {"list", ""})
	public String list(ShopStorehouse shopStorehouse, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopStorehouse> page = shopStorehouseService.findPage(new Page<ShopStorehouse>(request, response), shopStorehouse); 
		model.addAttribute("page", page);
		return "modules/shop/shopStorehouseList";
	}

	@RequiresPermissions("shop:shopStorehouse:view")
	@RequestMapping(value = "form")
	public String form(ShopStorehouse shopStorehouse, Model model) {
		model.addAttribute("shopStorehouse", shopStorehouse);
		return "modules/shop/shopStorehouseForm";
	}

	@RequiresPermissions("shop:shopStorehouse:edit")
	@RequestMapping(value = "save")
	public String save(ShopStorehouse shopStorehouse, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, shopStorehouse)){
			return form(shopStorehouse, model);
		}
		shopStorehouseService.save(shopStorehouse);
		addMessage(redirectAttributes, "保存仓库管理成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopStorehouse/?repage";
	}
	
	@RequiresPermissions("shop:shopStorehouse:edit")
	@RequestMapping(value = "delete")
	public String delete(ShopStorehouse shopStorehouse, RedirectAttributes redirectAttributes) {
		shopStorehouseService.delete(shopStorehouse);
		addMessage(redirectAttributes, "删除仓库管理成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopStorehouse/?repage";
	}

}