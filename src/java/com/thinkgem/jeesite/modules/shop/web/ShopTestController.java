/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopTest;
import com.thinkgem.jeesite.modules.shop.service.ShopTestService;

/**
 * 测试Controller
 * @author water
 * @version 2016-02-23
 */
@Controller
@RequestMapping(value = "${adminPath}/shop/shopTest")
public class ShopTestController extends BaseController {

	@Autowired
	private ShopTestService shopTestService;
	
	@ModelAttribute
	public ShopTest get(@RequestParam(required=false) String id) {
		ShopTest entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopTestService.get(id);
		}
		if (entity == null){
			entity = new ShopTest();
		}
		return entity;
	}
	
	@RequiresPermissions("shop:shopTest:view")
	@RequestMapping(value = {"list", ""})
	public String list(ShopTest shopTest, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopTest> page = shopTestService.findPage(new Page<ShopTest>(request, response), shopTest); 
		model.addAttribute("page", page);
		return "modules/shop/shopTestList";
	}

	@RequiresPermissions("shop:shopTest:view")
	@RequestMapping(value = "form")
	public String form(ShopTest shopTest, Model model) {
		model.addAttribute("shopTest", shopTest);
		return "modules/shop/shopTestForm";
	}

	@RequiresPermissions("shop:shopTest:edit")
	@RequestMapping(value = "save")
	public String save(ShopTest shopTest, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, shopTest)){
			return form(shopTest, model);
		}
		shopTestService.save(shopTest);
		addMessage(redirectAttributes, "保存测试成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopTest/?repage";
	}
	
	@RequiresPermissions("shop:shopTest:edit")
	@RequestMapping(value = "delete")
	public String delete(ShopTest shopTest, RedirectAttributes redirectAttributes) {
		shopTestService.delete(shopTest);
		addMessage(redirectAttributes, "删除测试成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopTest/?repage";
	}

}