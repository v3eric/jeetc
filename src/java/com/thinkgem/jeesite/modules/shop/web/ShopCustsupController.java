/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopCustsup;
import com.thinkgem.jeesite.modules.shop.service.ShopCustsupService;

/**
 * 客户厂商Controller
 * @author water
 * @version 2016-01-14
 */
@Controller
@RequestMapping(value = "${adminPath}/shop/shopCustsup")
public class ShopCustsupController extends BaseController {

	@Autowired
	private ShopCustsupService shopCustsupService;
	
	@ModelAttribute
	public ShopCustsup get(@RequestParam(required=false) String id) {
		ShopCustsup entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopCustsupService.get(id);
		}
		if (entity == null){
			entity = new ShopCustsup();
		}
		return entity;
	}
	
	@RequiresPermissions("shop:shopCustsup:view")
	@RequestMapping(value = {"list", ""})
	public String list(ShopCustsup shopCustsup, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopCustsup> page = shopCustsupService.findPage(new Page<ShopCustsup>(request, response), shopCustsup); 
		model.addAttribute("page", page);
		return "modules/shop/shopCustsupList";
	}

	@RequiresPermissions("shop:shopCustsup:view")
	@RequestMapping(value = "form")
	public String form(ShopCustsup shopCustsup, Model model) {
		model.addAttribute("shopCustsup", shopCustsup);
		return "modules/shop/shopCustsupForm";
	}

	@RequiresPermissions("shop:shopCustsup:edit")
	@RequestMapping(value = "save")
	public String save(ShopCustsup shopCustsup, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, shopCustsup)){
			return form(shopCustsup, model);
		}
		shopCustsupService.save(shopCustsup);
		addMessage(redirectAttributes, "保存客户厂商成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopCustsup/?repage";
	}
	
	@RequiresPermissions("shop:shopCustsup:edit")
	@RequestMapping(value = "delete")
	public String delete(ShopCustsup shopCustsup, RedirectAttributes redirectAttributes) {
		shopCustsupService.delete(shopCustsup);
		addMessage(redirectAttributes, "删除客户厂商成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopCustsup/?repage";
	}

}