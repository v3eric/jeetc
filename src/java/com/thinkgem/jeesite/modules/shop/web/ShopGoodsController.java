/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopGoods;
import com.thinkgem.jeesite.modules.shop.service.ShopGoodsService;

/**
 * 货品库存Controller
 * @author water
 * @version 2015-12-16
 */
@Controller
@RequestMapping(value = "${adminPath}/shop/shopGoods")
public class ShopGoodsController extends BaseController {

	@Autowired
	private ShopGoodsService shopGoodsService;
	
	@ModelAttribute
	public ShopGoods get(@RequestParam(required=false) String id) {
		ShopGoods entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopGoodsService.get(id);
		}
		if (entity == null){
			entity = new ShopGoods();
		}
		return entity;
	}
	
	@RequiresPermissions("shop:shopGoods:view")
	@RequestMapping(value = {"list", ""})
	public String list(ShopGoods shopGoods, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopGoods> page = shopGoodsService.findPage(new Page<ShopGoods>(request, response), shopGoods); 
		model.addAttribute("page", page);
		return "modules/shop/shopGoodsList";
	}

	@RequiresPermissions("shop:shopGoods:view")
	@RequestMapping(value = "form")
	public String form(ShopGoods shopGoods, Model model) {
		model.addAttribute("shopGoods", shopGoods);
		model.addAttribute("shopGoodsList", shopGoodsService.findList(new ShopGoods()));
//		System.out.println(shopGoodsService.findList(new ShopGoods()).size());
		return "modules/shop/shopGoodsForm";
	}

	@RequiresPermissions("shop:shopGoods:edit")
	@RequestMapping(value = "save")
	public String save(ShopGoods shopGoods, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, shopGoods)){
			return form(shopGoods, model);
		}
		shopGoodsService.save(shopGoods);
		addMessage(redirectAttributes, "保存产品保存成功成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopGoods/?repage";
	}
	
	@RequiresPermissions("shop:shopGoods:edit")
	@RequestMapping(value = "delete")
	public String delete(ShopGoods shopGoods, RedirectAttributes redirectAttributes) {
		shopGoodsService.delete(shopGoods);
		addMessage(redirectAttributes, "删除产品保存成功成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopGoods/?repage";
	}

}