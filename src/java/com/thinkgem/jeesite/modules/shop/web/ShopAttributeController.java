/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopAttribute;
import com.thinkgem.jeesite.modules.shop.entity.ShopBrand;
import com.thinkgem.jeesite.modules.shop.entity.ShopCategory;
import com.thinkgem.jeesite.modules.shop.entity.ShopPrdatt;
import com.thinkgem.jeesite.modules.shop.service.ShopAttributeService;
import com.thinkgem.jeesite.modules.shop.service.ShopCategoryService;
import com.thinkgem.jeesite.modules.shop.service.ShopPrdattService;

/**
 * 产品分类属性Controller
 * @author jjf
 * @version 2015-12-29
 */
@Controller
@RequestMapping(value = "${adminPath}/shop/shopAttribute")
public class ShopAttributeController extends BaseController {

	@Autowired
	private ShopAttributeService shopAttributeService;
	

	@Autowired
	private ShopCategoryService shopCategoryService;
	
	@Autowired
	private ShopPrdattService shopPrdattService;
	
	/**
	 * @param id
	 * @return
	 * /**
	 * @param id
	 * @retur @modelattribuute这种情况下，你调用任何一个它所在的类中的链接，即@requestMapping方法的时候都会首先执行这个方法，并且返回对象，
	 * 对象的标签就是该注解的value
	 */
	 
	@ModelAttribute
	public ShopAttribute get(@RequestParam(required=false) String id) {
		ShopAttribute entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopAttributeService.get(id);
		}
		if (entity == null){
			entity = new ShopAttribute();
		}
		return entity;
	}
	
	@RequiresPermissions("shop:shopAttribute:view")
	@RequestMapping(value = {"list", ""})
	public String list(ShopAttribute shopAttribute, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopAttribute> page = shopAttributeService.findPage(new Page<ShopAttribute>(request, response), shopAttribute); 
		//在这里看的mapping就是查询的  ShopAttribute的表头记录
		//System.out.println("======="+page.getList().get(0).getShopAttrivalueList().size());
		model.addAttribute("page", page);
		return "modules/shop/shopAttributeList";
	}

	@RequiresPermissions("shop:shopAttribute:view")
	@RequestMapping(value = "form")
	public String form(ShopAttribute shopAttribute, ShopCategory shopCategory ,Model model) {
		
		
		if (shopCategory.getParent()!=null && StringUtils.isNotBlank(shopCategory.getParent().getId())){
			shopCategory.setParent(shopCategoryService.get(shopCategory.getParent().getId()));
			// 获取排序号，最末节点排序号+30
			if (StringUtils.isBlank(shopCategory.getId())){
				ShopCategory shopCategoryChild = new ShopCategory();
				shopCategoryChild.setParent(new ShopCategory(shopCategory.getParent().getId()));
				List<ShopCategory> list = shopCategoryService.findList(shopCategory); 
				if (list.size() > 0){
					shopCategory.setSort(list.get(list.size()-1).getSort());
					if (shopCategory.getSort() != null){
						shopCategory.setSort(shopCategory.getSort() + 30);
					}	
				}
			}
		}
		
		
		
		if(shopAttribute.getShopCategory()!=null){
			if (shopAttribute.getShopCategory().getId()!=null && StringUtils.isNotBlank(shopAttribute.getShopCategory().getId())){
				shopAttribute.setShopCategory(shopCategoryService.get(shopAttribute.getShopCategory().getId()));
			}
		}

		if (shopCategory.getSort() == null){
			shopCategory.setSort(30);
		}
		model.addAttribute("shopCategory", shopCategory);
		model.addAttribute("shopPrdattList", shopPrdattService.findList(new ShopPrdatt()));
		model.addAttribute("shopAttribute", shopAttribute);
		return "modules/shop/shopAttributeForm";
	}

	@RequiresPermissions("shop:shopAttribute:edit")
	@RequestMapping(value = "save")
	public String save(ShopAttribute shopAttribute, ShopCategory shopCategory,Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, shopAttribute)){
			return form(shopAttribute,shopCategory, model);
		}
		shopAttributeService.save(shopAttribute);
		addMessage(redirectAttributes, "保存产品分类成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopAttribute/?repage";
	}
	
	@RequiresPermissions("shop:shopAttribute:edit")
	@RequestMapping(value = "delete")
	public String delete(ShopAttribute shopAttribute, RedirectAttributes redirectAttributes) {
		shopAttributeService.delete(shopAttribute);
		addMessage(redirectAttributes, "删除产品分类成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopAttribute/?repage";
	}
	
	@RequiresPermissions("shop:shopAttribute:view")
	@RequestMapping(value = "category")
	public String getAttributesByCategory(String id) {
		
		return "redirect:"+Global.getAdminPath()+"/shop/shopAttribute/?repage";
	}

}