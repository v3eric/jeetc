/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.cms.entity.Category;
import com.thinkgem.jeesite.modules.cms.entity.Site;
import com.thinkgem.jeesite.modules.shop.entity.ShopBrand;
import com.thinkgem.jeesite.modules.shop.entity.ShopCategory;
import com.thinkgem.jeesite.modules.shop.service.ShopBrandService;
import com.thinkgem.jeesite.modules.shop.service.ShopCategoryService;

/**
 * 产品品牌Controller
 * @author water
 * @version 2015-12-22
 */
@Controller
@RequestMapping(value = "${adminPath}/shop/shopBrand")
public class ShopBrandController extends BaseController {

	@Autowired
	private ShopBrandService shopBrandService;
	@Autowired
	private ShopCategoryService shopCategoryService;
	
	/**
	 * @param id
	 * @retur @modelattribuute这种情况下，你调用任何一个它所在的类中的链接，即@requestMapping方法的时候都会首先执行这个方法，并且返回对象，
	 * 对象的标签就是该注解的value
	 */
	@ModelAttribute
	public ShopBrand get(@RequestParam(required=false) String id) {
		ShopBrand entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopBrandService.get(id);
		}
		if (entity == null){
			entity = new ShopBrand();
		}
		return entity;
	}
	
	@RequiresPermissions("shop:shopBrand:view")
	@RequestMapping(value = {"list", ""})
	public String list(ShopBrand shopBrand, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopBrand> page = shopBrandService.findPage(new Page<ShopBrand>(request, response), shopBrand); 
		model.addAttribute("page", page);
		return "modules/shop/shopBrandList";
	}

	@RequiresPermissions("shop:shopBrand:view")
	@RequestMapping(value = "form")
	public String form(ShopBrand shopBrand,ShopCategory shopCategory,Model model) {
		
		if (shopCategory.getParent()!=null && StringUtils.isNotBlank(shopCategory.getParent().getId())){
			shopCategory.setParent(shopCategoryService.get(shopCategory.getParent().getId()));
			// 获取排序号，最末节点排序号+30
			if (StringUtils.isBlank(shopCategory.getId())){
				ShopCategory shopCategoryChild = new ShopCategory();
				shopCategoryChild.setParent(new ShopCategory(shopCategory.getParent().getId()));
				List<ShopCategory> list = shopCategoryService.findList(shopCategory); 
				if (list.size() > 0){
					shopCategory.setSort(list.get(list.size()-1).getSort());
					if (shopCategory.getSort() != null){
						shopCategory.setSort(shopCategory.getSort() + 30);
					}
				}
			}
		}
		
		
		
		if(shopBrand.getShopCategory()!=null){
			if (shopBrand.getShopCategory().getId()!=null && StringUtils.isNotBlank(shopBrand.getShopCategory().getId())){
				shopBrand.setShopCategory(shopCategoryService.get(shopBrand.getShopCategory().getId()));
			}
		}
	
		
		
		
		
		if (shopCategory.getSort() == null){
			shopCategory.setSort(30);
		}
		model.addAttribute("shopCategory", shopCategory);
		model.addAttribute("shopBrand", shopBrand);
//		System.out.println("======="+shopBrand.getShopCategoryId().getName());
		return "modules/shop/shopBrandForm";
	}

	@RequiresPermissions("shop:shopBrand:edit")
	@RequestMapping(value = "save")
	public String save(ShopBrand shopBrand, ShopCategory shopCategory,Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, shopBrand)){
			return form(shopBrand, shopCategory,model);
		}
		shopBrandService.save(shopBrand);
		addMessage(redirectAttributes, "保存品牌成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopBrand/?repage";
	}
	
	@RequiresPermissions("shop:shopBrand:edit")
	@RequestMapping(value = "delete")
	public String delete(ShopBrand shopBrand, RedirectAttributes redirectAttributes) {
		shopBrandService.delete(shopBrand);
		addMessage(redirectAttributes, "删除品牌成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopBrand/?repage";
	}

}