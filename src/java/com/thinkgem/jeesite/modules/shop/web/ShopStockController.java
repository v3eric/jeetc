/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopCustsup;
import com.thinkgem.jeesite.modules.shop.entity.ShopPrdmark;
import com.thinkgem.jeesite.modules.shop.entity.ShopProduct;
import com.thinkgem.jeesite.modules.shop.entity.ShopStock;
import com.thinkgem.jeesite.modules.shop.service.ShopCustsupService;
import com.thinkgem.jeesite.modules.shop.service.ShopPrdmarkService;
import com.thinkgem.jeesite.modules.shop.service.ShopProductService;
import com.thinkgem.jeesite.modules.shop.service.ShopStockService;

/**
 * 库存Controller
 * @author water
 * @version 2016-01-15
 */
@Controller
@RequestMapping(value = "${adminPath}/shop/shopStock")
public class ShopStockController extends BaseController {

	@Autowired
	private ShopStockService shopStockService;
	
	@Autowired
	private ShopCustsupService shopCustsupService;
	
	@Autowired
	private ShopProductService shopProductService;
	
	@Autowired
	private ShopPrdmarkService shopPrdmarkService;
	
	@ModelAttribute
	public ShopStock get(@RequestParam(required=false) String id) {
		ShopStock entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopStockService.get(id);
		}
		if (entity == null){
			entity = new ShopStock();
		}
		return entity;
	}
	
	@RequiresPermissions("shop:shopStock:view")
	@RequestMapping(value = {"list", ""})
	public String list(ShopStock shopStock, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopStock> page = shopStockService.findPage(new Page<ShopStock>(request, response), shopStock); 
		model.addAttribute("page", page);
		
		return "modules/shop/shopStockList";
	}
	

	
	@RequiresPermissions("shop:shopStock:view")
	@RequestMapping(value = "form")
	public String form(ShopStock shopStock, Model model) {

		model.addAttribute("shopProductList", shopProductService.findList(new ShopProduct()));
		model.addAttribute("shopPrdmarkList", shopPrdmarkService.findList(new ShopPrdmark()));
		model.addAttribute("shopCustsupList", shopCustsupService.findList(new ShopCustsup()));
		model.addAttribute("shopStock", shopStock);
//		System.out.println(shopStockService.sumqty().size());
		return "modules/shop/shopStockForm";
	}
	
	@RequiresPermissions("shop:shopStock:view")
	@RequestMapping(value = "sumqty")
	public String sumqty(ShopStock shopStock, Model model) {

//		System.out.println(shopStockService.sumqty().size());
		model.addAttribute("shopStockqtyList", shopStockService.sumqty());
		return "modules/shop/shopStockqtyList";
	}

	@RequiresPermissions("shop:shopStock:edit")
	@RequestMapping(value = "save")
	public String save(ShopStock shopStock, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, shopStock)){
			return form(shopStock, model);
		}
		shopStockService.save(shopStock);
		addMessage(redirectAttributes, "保存库存成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopStock/?repage";
	}
	
	@RequiresPermissions("shop:shopStock:edit")
	@RequestMapping(value = "delete")
	public String delete(ShopStock shopStock, RedirectAttributes redirectAttributes) {
		shopStockService.delete(shopStock);
		addMessage(redirectAttributes, "删除库存成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopStock/?repage";
	}

}