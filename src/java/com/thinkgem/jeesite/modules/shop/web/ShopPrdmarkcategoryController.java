/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.shop.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.shop.entity.ShopPrdmarkcategory;
import com.thinkgem.jeesite.modules.shop.service.ShopPrdmarkcategoryService;

/**
 * 特征分类Controller
 * @author water
 * @version 2016-09-06
 */
@Controller
@RequestMapping(value = "${adminPath}/shop/shopPrdmarkcategory")
public class ShopPrdmarkcategoryController extends BaseController {

	@Autowired
	private ShopPrdmarkcategoryService shopPrdmarkcategoryService;
	
	@ModelAttribute
	public ShopPrdmarkcategory get(@RequestParam(required=false) String id) {
		ShopPrdmarkcategory entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopPrdmarkcategoryService.get(id);
		}
		if (entity == null){
			entity = new ShopPrdmarkcategory();
		}
		return entity;
	}
	
	@RequiresPermissions("shop:shopPrdmarkcategory:view")
	@RequestMapping(value = {"list", ""})
	public String list(ShopPrdmarkcategory shopPrdmarkcategory, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopPrdmarkcategory> page = shopPrdmarkcategoryService.findPage(new Page<ShopPrdmarkcategory>(request, response), shopPrdmarkcategory); 
		model.addAttribute("page", page);
		return "modules/shop/shopPrdmarkcategoryList";
	}

	@RequiresPermissions("shop:shopPrdmarkcategory:view")
	@RequestMapping(value = "form")
	public String form(ShopPrdmarkcategory shopPrdmarkcategory, Model model) {
		model.addAttribute("shopPrdmarkcategory", shopPrdmarkcategory);
		return "modules/shop/shopPrdmarkcategoryForm";
	}

	@RequiresPermissions("shop:shopPrdmarkcategory:edit")
	@RequestMapping(value = "save")
	public String save(ShopPrdmarkcategory shopPrdmarkcategory, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, shopPrdmarkcategory)){
			return form(shopPrdmarkcategory, model);
		}
		shopPrdmarkcategoryService.save(shopPrdmarkcategory);
		addMessage(redirectAttributes, "保存特征分类成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopPrdmarkcategory/?repage";
	}
	
	@RequiresPermissions("shop:shopPrdmarkcategory:edit")
	@RequestMapping(value = "delete")
	public String delete(ShopPrdmarkcategory shopPrdmarkcategory, RedirectAttributes redirectAttributes) {
		shopPrdmarkcategoryService.delete(shopPrdmarkcategory);
		addMessage(redirectAttributes, "删除特征分类成功");
		return "redirect:"+Global.getAdminPath()+"/shop/shopPrdmarkcategory/?repage";
	}

}